


    if (typeof jQuery !== 'undefined' && typeof jQuery.fn.DataTable !== 'undefined') {
        $(document).ready(function(){
            
            const baseUrlElement = document.getElementById('base-url');
            const baseUrl        = baseUrlElement.getAttribute('data-baseurl');

            $.ajaxSetup({
                headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }        
            })

            // Datatable Peserta
            $('#table-paket').DataTable({
                "language": {
                "lengthMenu": "Show _MENU_",
                },
                "dom":
                "<'row'" +
                "<'col-sm-6 d-flex align-items-center justify-conten-start'l>" +
                "<'col-sm-6 d-flex align-items-center justify-content-end'f>" +
                ">" +
            
                "<'table-responsive'tr>" +
            
                "<'row'" +
                "<'col-sm-12 col-md-5 d-flex align-items-center justify-content-center justify-content-md-start'i>" +
                "<'col-sm-12 col-md-7 d-flex align-items-center justify-content-center justify-content-md-end'p>" +
                ">",
                processing: true,
                serverSide: true,
                pageLength:  50,
                language: {
                    processing: 'Sedang memuat data...',
                    searchPlaceholder: "Cari data ..."
                },
                ajax: {
                url: baseUrl + '/sistem/table-data-paket',
                error: function (xhr, error, thrown) {
                    toastNotif('error', 'Gagal Melakukan load data dari sistem, pastikan internet anda lancar', 'GAGAL LOAD DATA', )}
                },
                columns: [
                    { data: 'DT_RowIndex', name: '#', 'className': 'text-center'},
                    { data: 'jenis_peserta', name: 'Jenis Peserta'},
                    { data: 'nama_paket', name: 'Nama Paket'},
                    { data: 'harga', name: 'Harga Paket', 'className': 'text-right'},
                    { data: 'status', name: 'Status', 'className': 'text-center'},
                    { data: 'waktu', name: 'waktu', 'className': 'text-center'},
                    { data: 'aksi', name: 'Aksi', orderable: false, searchable: false , 'className': 'text-center'},
                ],
                
            });

            $('#table-datauser').DataTable({
                "language": {
                "lengthMenu": "Show _MENU_",
                },
                "dom":
                "<'row'" +
                "<'col-sm-6 d-flex align-items-center justify-conten-start'l>" +
                "<'col-sm-6 d-flex align-items-center justify-content-end'f>" +
                ">" +
            
                "<'table-responsive'tr>" +
            
                "<'row'" +
                "<'col-sm-12 col-md-5 d-flex align-items-center justify-content-center justify-content-md-start'i>" +
                "<'col-sm-12 col-md-7 d-flex align-items-center justify-content-center justify-content-md-end'p>" +
                ">",
                processing: true,
                serverSide: true,
                pageLength:  50,
                language: {
                    processing: 'Sedang memuat data...',
                    searchPlaceholder: "Cari data ..."
                },
                ajax: {
                url: baseUrl + '/sistem/table-data-users',
                error: function (xhr, error, thrown) {
                    toastNotif('error', 'Gagal Melakukan load data dari sistem, pastikan internet anda lancar', 'GAGAL LOAD DATA', )}
                },
                columns: [
                    { data: 'DT_RowIndex', name: '#', 'className': 'text-center'},
                    { data: 'nama_lengkap', name: 'Nama Lengkap'},
                    { data: 'email', name: 'Email'},
                    { data: 'level_akun', name: 'Level Akun', 'className': 'text-center'},
                    { data: 'status_akun', name: 'Status', 'className': 'text-center'},
                    { data: 'aksi', name: 'Aksi', orderable: false, searchable: false , 'className': 'text-center'},
                ],
                
            });

            $('#table-peserta-abs').DataTable({
                "language": {
                "lengthMenu": "Show _MENU_",
                },
                "dom":
                "<'row'" +
                "<'col-sm-6 d-flex align-items-center justify-conten-start'l>" +
                "<'col-sm-6 d-flex align-items-center justify-content-end'f>" +
                ">" +
            
                "<'table-responsive'tr>" +
            
                "<'row'" +
                "<'col-sm-12 col-md-5 d-flex align-items-center justify-content-center justify-content-md-start'i>" +
                "<'col-sm-12 col-md-7 d-flex align-items-center justify-content-center justify-content-md-end'p>" +
                ">",
                processing: true,
                serverSide: true,
                pageLength:  50,
                language: {
                    processing: 'Sedang memuat data...',
                    searchPlaceholder: "Cari data ..."
                },
                ajax: {
                url: baseUrl + '/sistem/table-data-abs',
                error: function (xhr, error, thrown) {
                    toastNotif('error', 'Gagal Melakukan load data dari sistem, pastikan internet anda lancar', 'GAGAL LOAD DATA', )}
                },
                columns: [
                    { data: 'DT_RowIndex', name: '#', 'className': 'text-center'},
                    { data: 'pst_id', name: 'Id Peserta', 'className': 'text-center'},
                    { data: 'nama_lengkap', name: 'Nama Lengkap'},
                    { data: 'created_at', name: 'Status', 'className': 'text-center'},
                    { data: 'file_abs', name: 'Status', 'className': 'text-center'},
                    // { data: 'aksi', name: 'Aksi', orderable: false, searchable: false , 'className': 'text-center'},
                ],
                
            });

            $('#table-pendaftaran').DataTable({
                "language": {
                "lengthMenu": "Show _MENU_",
                },
                "dom":
                "<'row'" +
                "<'col-sm-6 d-flex align-items-center justify-conten-start'l>" +
                "<'col-sm-6 d-flex align-items-center justify-content-end'f>" +
                ">" +
            
                "<'table-responsive'tr>" +
            
                "<'row'" +
                "<'col-sm-12 col-md-5 d-flex align-items-center justify-content-center justify-content-md-start'i>" +
                "<'col-sm-12 col-md-7 d-flex align-items-center justify-content-center justify-content-md-end'p>" +
                ">",
                processing: true,
                serverSide: true,
                pageLength:  50,
                language: {
                    processing: 'Sedang memuat data...',
                    searchPlaceholder: "Cari data ..."
                },
                ajax: {
                url: baseUrl + '/sistem/table-data-pendaftaran',
                error: function (xhr, error, thrown) {
                    toastNotif('error', 'Gagal Melakukan load data dari sistem, pastikan internet anda lancar', 'GAGAL LOAD DATA', )}
                },
                columns: [
                    { data: 'DT_RowIndex', name: '#', 'className': 'text-center', orderable: false, searchable: false},
                    { data: 'kode_invoice', name: 'kode_invoice', 'className': 'text-center', orderable: true, searchable: true},
                    { data: 'kode_unik', name: 'kode_unik', 'className': 'text-center', orderable: true, searchable: true},
                    { data: 'total_biaya', name: 'total_biaya', 'className': 'text-right', orderable: true, searchable: true},
                    { data: 'created_at', name: 'created_at', 'className': 'text-center', orderable: true, searchable: false},
                    { data: 'status_pembayaran', name: 'status_pembayaran', 'className': 'text-center', orderable: true, searchable: false},
                    { data: 'notifikasi', name: 'notifikasi', 'className': 'text-center', orderable: false, searchable: false},
                    { data: 'file_bukti', name: 'file_bukti', 'className': 'text-center', orderable: false, searchable: false},
                    { data: 'aksi', name: 'aksi', orderable: false, searchable: false , 'className': 'text-center', orderable: false, searchable: false},
                ],
                columnDefs: [
                    {
                        targets: [1, 2, 3, 4, 5, 6, 7], // Kolom yang ingin Anda izinkan untuk dicari
                        searchable: true
                    }
                ]
            });

            $('#table-peserta').DataTable({
                "language": {
                "lengthMenu": "Show _MENU_",
                },
                "dom":
                "<'row'" +
                "<'col-sm-6 d-flex align-items-center justify-conten-start'l>" +
                "<'col-sm-6 d-flex align-items-center justify-content-end'f>" +
                ">" +
            
                "<'table-responsive'tr>" +
            
                "<'row'" +
                "<'col-sm-12 col-md-5 d-flex align-items-center justify-content-center justify-content-md-start'i>" +
                "<'col-sm-12 col-md-7 d-flex align-items-center justify-content-center justify-content-md-end'p>" +
                ">",
                processing: true,
                serverSide: true,
                pageLength:  50,
                language: {
                    processing: 'Sedang memuat data...',
                    searchPlaceholder: "Cari data ..."
                },
                ajax: {
                url: baseUrl + '/sistem/table-data-peserta',
                error: function (xhr, error, thrown) {
                    toastNotif('error', 'Gagal Melakukan load data dari sistem, pastikan internet anda lancar', 'GAGAL LOAD DATA', )}
                },
                order: [[2, 'asc']],
                columns: [
                    { data: 'DT_RowIndex', name: '#', 'className': 'text-center'},
                    { data: 'pst_id', name: 'pst_id', 'className': 'text-center'},
                    { data: 'kode_invoice', name: 'kode_invoice', 'className': 'text-center'},
                    { data: 'nama_lengkap', name: 'nama_lengkap', 'className': 'text-left'},
                    { data: 'jenis_peserta', name: 'jenis_peserta', 'className': 'text-left'},
                    { data: 'nama_paket', name: 'nama_paket', 'className': 'text-left'},
                    { data: 'notif', name: 'notif', 'className': 'text-center', orderable: false},
                    { data: 'aksi', name: 'Aksi', orderable: false, searchable: false , 'className': 'text-center'},
                ],
                createdRow: function (row, data, dataIndex) {
                    $(row).addClass('detail-peserta');
                    $(row).attr('data-idpeserta', data.peserta_id);
                },
            });

            $('#table-hotel').DataTable({
                "language": {
                "lengthMenu": "Show _MENU_",
                },
                "dom":
                "<'row'" +
                "<'col-sm-6 d-flex align-items-center justify-conten-start'l>" +
                "<'col-sm-6 d-flex align-items-center justify-content-end'f>" +
                ">" +
            
                "<'table-responsive'tr>" +
            
                "<'row'" +
                "<'col-sm-12 col-md-5 d-flex align-items-center justify-content-center justify-content-md-start'i>" +
                "<'col-sm-12 col-md-7 d-flex align-items-center justify-content-center justify-content-md-end'p>" +
                ">",
                processing: true,
                serverSide: true,
                pageLength:  50,
                language: {
                    processing: 'Sedang memuat data...',
                    searchPlaceholder: "Cari data ..."
                },
                ajax: {
                url: baseUrl + '/sistem/table-data-hotel',
                error: function (xhr, error, thrown) {
                    toastNotif('error', 'Gagal Melakukan load data dari sistem, pastikan internet anda lancar', 'GAGAL LOAD DATA', )}
                },
                order: [[2, 'asc']],
                columns: [
                    { data: 'DT_RowIndex', name: '#', 'className': 'align-middle text-center'},
                    { data: 'nama_hotel', name: 'Nama Hotel', 'className': 'align-middle text-left'},
                    { data: 'gambar_hotel', name: 'Gambar Hotel', 'className': 'align-middle text-center'},
                    { data: 'rate_bintang', name: 'Rate Bintang', 'className': 'align-middle text-center'},
                    { data: 'status', name: 'Status', 'className': 'align-middle text-center'},
                    { data: 'alamat', name: 'Alamat', 'className': 'align-middle text-left'},
                    { data: 'aksi', name: 'Aksi', orderable: false, searchable: false , 'className': 'align-middle text-center'},
                ]
            });

            $('#table-paket-hotel').DataTable({
                "language": {
                "lengthMenu": "Show _MENU_",
                },
                "dom":
                "<'row'" +
                "<'col-sm-6 d-flex align-items-center justify-conten-start'l>" +
                "<'col-sm-6 d-flex align-items-center justify-content-end'f>" +
                ">" +
            
                "<'table-responsive'tr>" +
            
                "<'row'" +
                "<'col-sm-12 col-md-5 d-flex align-items-center justify-content-center justify-content-md-start'i>" +
                "<'col-sm-12 col-md-7 d-flex align-items-center justify-content-center justify-content-md-end'p>" +
                ">",
                processing: true,
                serverSide: true,
                pageLength:  50,
                language: {
                    processing: 'Sedang memuat data...',
                    searchPlaceholder: "Cari data ..."
                },
                ajax: {
                url: baseUrl + '/sistem/table-data-pakethotel',
                error: function (xhr, error, thrown) {
                    toastNotif('error', 'Gagal Melakukan load data dari sistem, pastikan internet anda lancar', 'GAGAL LOAD DATA', )}
                },
                order: [[2, 'asc']],
                columns: [
                    { data: 'DT_RowIndex', name: '#', 'className': 'align-middle text-center'},
                    { data: 'nama_hotel', name: 'Nama Hotel', 'className': 'align-middle text-left'},
                    { data: 'nama_paket', name: 'Nama Paket', 'className': 'align-middle text-left'},
                    { data: 'harga_paket', name: 'Harga Paket', 'className': 'align-middle text-right'},
                    { data: 'status_paket', name: 'Status', 'className': 'align-middle text-center'},
                    { data: 'aksi', name: 'Aksi', orderable: false, searchable: false , 'className': 'align-middle text-center'},
                ]
            });

            $('#table-reservasi-hotel').DataTable({
                "language": {
                "lengthMenu": "Show _MENU_",
                },
                "dom":
                "<'row'" +
                "<'col-sm-6 d-flex align-items-center justify-conten-start'l>" +
                "<'col-sm-6 d-flex align-items-center justify-content-end'f>" +
                ">" +
            
                "<'table-responsive'tr>" +
            
                "<'row'" +
                "<'col-sm-12 col-md-5 d-flex align-items-center justify-content-center justify-content-md-start'i>" +
                "<'col-sm-12 col-md-7 d-flex align-items-center justify-content-center justify-content-md-end'p>" +
                ">",
                processing: true,
                serverSide: true,
                pageLength:  50,
                language: {
                    processing: 'Sedang memuat data...',
                    searchPlaceholder: "Cari data ..."
                },
                ajax: {
                url: baseUrl + '/sistem/table-data-reservasihotel',
                error: function (xhr, error, thrown) {
                    toastNotif('error', 'Gagal Melakukan load data dari sistem, pastikan internet anda lancar', 'GAGAL LOAD DATA', )}
                },
                order: [[2, 'asc']],
                columns: [
                    { data: 'DT_RowIndex', name: '#', 'className': 'align-middle text-center'},
                    { data: 'nama_lengkap', name: 'Nama Lengkap', 'className': 'align-middle text-left'},
                    { data: 'detail_paket', name: 'Hotel & Nama Paket', 'className': 'align-middle text-left'},
                    { data: 'detail_harga', name: 'Harga & Total Biaya', 'className': 'align-middle text-right'},
                    { data: 'detail_cico', name: 'CheckIn / CheckOut', 'className': 'align-middle text-right'},
                    { data: 'status_res', name: 'Status', 'className': 'align-middle text-center'},
                    { data: 'aksi', name: 'Aksi', orderable: false, searchable: false , 'className': 'align-middle text-center'},
                ]
            });


            
        });
    }