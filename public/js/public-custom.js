$(document).ready(function(){

    // $('.jeniskamar').select2();
    var hash = window.location.hash;

    // Memeriksa jika hash ada dan memiliki format yang sesuai
    // if (hash && hash.indexOf('#') !== -1) {
    //     // Memisahkan hash berdasarkan karakter '#'
    //     var parts = hash.split('#');
        
    //     // Menentukan id target dari hash
    //     var targetId = parts[2];                
    //     if (targetId == 'tata-cara') {
    //         $('#tata-cara').collapse('show');            
    //     }
    //     else if (targetId == 'registrasi') {
    //         $('#registrasi').collapse('show');            
    //     }
    //     else if (targetId == 'bukti-pembayaran') {
    //         $('#bukti-pembayaran').collapse('show');            
    //     }
    //     else if (targetId == 'notifikasi-email') {
    //         $('#notifikasi-email').collapse('show');            
    //     }
        
    // }


   

    $('#jenis-partisipasi').change(function(){
        var jenisPeserta = $(this).val();
        var jenisKegiatan;

        const baseUrlElement = document.getElementById('base-url');
        const baseUrl        = baseUrlElement.getAttribute('data-baseurl');
        
        $.ajax({
            url: baseUrl + '/sistem/harga-paket',
            type: 'GET',
            dataType:"json",
            data: { peserta: jenisPeserta },
            success: function (data) 
            {
                $('#jenis-kegiatan').empty();
                $('#total-biaya').val('');
                $('#jenis-kegiatan').append(data);
            },
            error: function (xhr) {
                if (xhr.status === 404) {
                    var responseData = JSON.parse(xhr.responseText);
                    var message      = responseData.message;
                    Swal.fire({
                        title: 'GAGAL',
                        text: message,
                        type: 'error',
                        confirmButtonColor: "#348cd4"
                    });
                }
            }
        });
    });

    $('#jenis-kegiatan').change(function(){
        const hargaKegiatan = $('#jenis-kegiatan option:selected').data('harga');
        $('#total-biaya').val(formatRupiah(hargaKegiatan));
    });

    function formatRupiah(angka) {
        var reverse = angka.toString().split('').reverse().join('');
        var ribuan = reverse.match(/\d{1,3}/g);
        var formatted = ribuan.join('.').split('').reverse().join('');
        return 'Rp ' + formatted;
    }

    $("#form-registrasi").submit(function (event) {
        event.preventDefault();

        

        const baseUrlElement = document.getElementById('base-url');
        const baseUrl        = baseUrlElement.getAttribute('data-baseurl');
        var formData         = new FormData(this);

        
        Swal.fire({
            title: 'Registrasi',
            html: "Apakah anda ingin mengkonfirmasi pendaftaran ? <br> Pastikan alamat email yang anda inputkan aktif & benar",
            type: 'question',
            showCancelButton: true,
            cancelButtonColor: '#FF3D60',
            confirmButtonText: 'Ya, registrasi',
        }).then(function (t){
            if (t.value) 
            {
                showLoading();
                $.ajax({
                    url: baseUrl + '/registrasi-peserta',
                    type: "post",
                    data: formData,
                    dataType: 'json',
                    contentType: false,
                    cache: false,
                    processData: false,
                    success: function (response) {

                        // document.getElementById("jenis-peserta").selectedIndex = -1;
                        $('#jenis-kegiatan').empty();

                        $('#total-biaya').val('');
                        $('#gelar-depan').val('');
                        $('#gelar-belakang').val('');
                        $('#nama-lengkap-peserta').val('');
                        $('#email').val('');
                        $('#nohp').val('');
                        $('#instansi-asal').val('');

                        Swal.fire({
                            title: response.title,
                            text: response.message,
                            type: response.statuslog,
                            confirmButtonColor: "#348cd4"
                        });
                        
                    },
                    complete: function(){
                        hideLoading();
                    },
                    error: function (xhr) {
                        var responseData = JSON.parse(xhr.responseText);
                        var message      = responseData.message;
                        Swal.fire({
                            title: 'GAGAL',
                            text: message,
                            type: 'error',
                            confirmButtonColor: "#348cd4"
                        });
                    }
                });
            }
        })
    });

    hideLoading();

    $("#form-notifikasi-ulang").submit(function (event) {
        event.preventDefault();
    
        const baseUrlElement = document.getElementById('base-url');
        const baseUrl        = baseUrlElement.getAttribute('data-baseurl');
        var formData         = new FormData(this);

        Swal.fire({
            title: 'Notifikasi Ulang',
            text: "Apakah anda ingin notifikasi ulang email ?",
            type: 'question',
            showCancelButton: true,
            cancelButtonColor: '#FF3D60',
            confirmButtonText: 'Ya, Notifikasi ulang email'
        }).then(function (t){
            if (t.value) 
            {
                showLoading();
                $.ajax({
                    url: baseUrl + '/notifikasi-ulang',
                    type: "post",
                    data: formData,
                    dataType: 'json',
                    contentType: false,
                    cache: false,
                    processData: false,
                    success: function (response) {
                        Swal.fire({
                            title: response.title,
                            text: response.message,
                            type: response.statuslog,
                            confirmButtonColor: "#348cd4"
                        });
                    },
                    complete: function(){
                        hideLoading();
                    },
                    error: function (xhr) {
                        var responseData = JSON.parse(xhr.responseText);
                        var message      = responseData.message;
                        Swal.fire({
                            title: 'GAGAL',
                            text: message,
                            type: 'error',
                            confirmButtonColor: "#348cd4"
                        });
                    }
                });
            }
        })
                
    
    });

    $('#kode_invoice').change(function(){
        var kode_invoice = $(this).val();

        const baseUrlElement = document.getElementById('base-url');
        const baseUrl        = baseUrlElement.getAttribute('data-baseurl');
        $('#dtl-namalengkap').val('');
        $('#dtl-jeniskegiatan').val('');
        $('#dtl-totalbiaya').val('');

        $.ajax({
            url: baseUrl + '/detail-invoice',
            type: 'GET',
            dataType:"json",
            data: { invoice: kode_invoice },
            success: function (data) 
            {
                $('#dtl-namalengkap').val(data.nama_lengkap);
                $('#dtl-jeniskegiatan').val(data.nama_paket);
                $('#dtl-totalbiaya').val(data.total_biaya);

            },
            error: function (xhr) {
                if (xhr.status === 404) {
                    var responseData = JSON.parse(xhr.responseText);
                    var message = responseData.message;
                    Swal.fire({
                            title: responseData.title,
                            text: responseData.message,
                            type: responseData.statuslog,
                            confirmButtonColor: "#348cd4"
                    });
                    
                }
            }
        });
    });
    

    $("#form-uploadpembayaran").submit(function (event) {
        event.preventDefault();
    
        const baseUrlElement = document.getElementById('base-url');
        const baseUrl        = baseUrlElement.getAttribute('data-baseurl');
        var formData         = new FormData(this);

        Swal.fire({
            title: 'Upload Bukti Pembayaran',
            text: "Apakah anda ingin mengupload bukti ?",
            type: 'question',
            showCancelButton: true,
            cancelButtonColor: '#FF3D60',
            confirmButtonText: 'Ya, Upload'
        }).then(function (t){
            if (t.value) 
            {
                showLoading();
                $.ajax({
                    url: baseUrl + '/upload-bukti-bayar',
                    type: "post",
                    data: formData,
                    dataType: 'json',
                    contentType: false,
                    cache: false,
                    processData: false,
                    success: function (response) {
                        Swal.fire({
                            title: response.title,
                            text: response.message,
                            type: response.statuslog,
                            confirmButtonColor: "#348cd4"
                        });
                    },
                    complete: function(){
                        hideLoading();
                    },
                    error: function (xhr) {
                        var responseData = JSON.parse(xhr.responseText);
                        var message      = responseData.message;
                        Swal.fire({
                            title: 'GAGAL',
                            text: message,
                            type: 'error',
                            confirmButtonColor: "#348cd4"
                        });
                    }
                });
            }
        })
                
    
    });

    $("#form-reservasi-hotel").submit(function (event) {
        event.preventDefault();
    
        const baseUrlElement = document.getElementById('base-url');
        const baseUrl        = baseUrlElement.getAttribute('data-baseurl');
        var formData         = new FormData(this);

        Swal.fire({
            title: 'Konfirmasi Reservasi Hotel',
            text: "Apakah anda ingin melakukan reservasi hotel ?",
            type: 'question',
            showCancelButton: true,
            cancelButtonColor: '#FF3D60',
            confirmButtonText: 'Ya, Reservasi'
        }).then(function (t){
            if (t.value) 
            {
                showLoading();
                $.ajax({
                    url: baseUrl + '/reservasi-hotel',
                    type: "post",
                    data: formData,
                    dataType: 'json',
                    contentType: false,
                    cache: false,
                    processData: false,
                    customClass: {
                        popup: 'custom-popup-class',
                    },
                    success: function (response) {
                        $('#modal-reservasi').modal('hide');
                        document.getElementById("form-reservasi-hotel").reset();
                        document.getElementById("filestruct").value = '';
                        $('#jeniskamar').empty();

                        Swal.fire({
                            title: response.title,
                            text: response.message,
                            type: response.statuslog,
                            confirmButtonColor: "#348cd4"
                        });
                    },
                    complete: function(){
                        hideLoading();
                    },
                    error: function (xhr) {
                        var responseData = JSON.parse(xhr.responseText);
                        var message      = responseData.message;
                        Swal.fire({
                            title: 'GAGAL',
                            text: message,
                            type: 'error',
                            confirmButtonColor: "#348cd4"
                        });
                    }
                    
                });
            }
        })
                
    
    });

    $('#noregpeserta').change(function(){
        var noreg = $(this).val();
        var jenisKegiatan;

        const baseUrlElement = document.getElementById('base-url');
        const baseUrl        = baseUrlElement.getAttribute('data-baseurl');
        
        $.ajax({
            url: baseUrl + '/sistem/noreg-peserta',
            type: 'GET',
            dataType:"json",
            data: { peserta: noreg },
            success: function (data) 
            {
                $('#namalengkap_abs').val(data.nama_lengkap);
                $('#email_abs').val(data.email);
                $('#nohp_abs').val(data.nohp);

            },
            error: function (xhr) {
                if (xhr.status === 404) {
                    var responseData = JSON.parse(xhr.responseText);
                    var message      = responseData.message;
                    Swal.fire({
                        title: 'GAGAL',
                        text: message,
                        type: 'error',
                        confirmButtonColor: "#348cd4"
                    });
                }
            }
        });
    });


    $("#form-abstract").submit(function (event) {
        event.preventDefault();
    
        const baseUrlElement = document.getElementById('base-url');
        const baseUrl        = baseUrlElement.getAttribute('data-baseurl');
        var formData         = new FormData(this);

        Swal.fire({
            title: 'Registrasi Abstract',
            text: "Apakah anda mengikuti pendaftaran Call for Abstract ?",
            type: 'question',
            showCancelButton: true,
            cancelButtonColor: '#FF3D60',
            confirmButtonText: 'Ya, mengikuti'
        }).then(function (t){
            if (t.value) 
            {
                showLoading();
                $.ajax({
                    url: baseUrl + '/registrasi-abstract',
                    type: "post",
                    data: formData,
                    dataType: 'json',
                    contentType: false,
                    cache: false,
                    processData: false,
                    success: function (response) {
                        document.getElementById("form-abstract").reset();
                        document.getElementById("regfile-abs").value = '';
                        $('#modal-3').modal('hide');
                        Swal.fire({
                            title: response.title,
                            text: response.message,
                            type: response.statuslog,
                            confirmButtonColor: "#348cd4"
                        });
                    },
                    complete: function(){
                        hideLoading();
                    },
                    error: function (xhr) {
                        var responseData = JSON.parse(xhr.responseText);
                        var message      = responseData.message;
                        // Disini harusnya bukan toastNotification
                        Swal.fire({
                            title: 'GAGAL',
                            text: message,
                            type: 'error',
                            confirmButtonColor: "#348cd4"
                        });
                    }
                });
            }
        })
                
    
    });

    function hideLoading(){
        $("#loading-ajax").fadeOut();
    }

    // Fungsi tutup loading
    function showLoading(){
        $("#loading-ajax").fadeIn();
    }

    $('#idpesertaripiu').on('input', function() {
        var idpesertaripiu   = $(this).val();
        var elementNotif        = document.getElementById('notif-alertserti');
        var elementNotif2        = document.getElementById('notif-alertsertisuccess');

        document.getElementById('dtl-namalengkap').value   = '';
        document.getElementById('dtl-jeniskegiatan').value = '';
        
        if (elementNotif) {
            if (!elementNotif.classList.contains('d-none')) {
                elementNotif.classList.add('d-none');
            }
        }

        if (elementNotif2) {
            if (!elementNotif2.classList.contains('d-none')) {
                elementNotif2.classList.add('d-none');
            }
        }

        if (idpesertaripiu.length >= 10) {
            const baseUrlElement = document.getElementById('base-url');
            const baseUrl        = baseUrlElement.getAttribute('data-baseurl');

            $.ajax({
                url: baseUrl + '/detail-peserta',
                type: 'GET',
                dataType: "json",
                data: { idpesertaripiu: idpesertaripiu},
                success: function(data) {

                    document.getElementById("dtl-namalengkap").value   = data.nama_lengkap;
                    document.getElementById("dtl-jeniskegiatan").value = data.nama_paket;
                    document.getElementById('btn-cetakserti').classList.remove('disabled');
                    
                },
                error: function(xhr) {
                    if (xhr.status === 404) {
                        var responseData = JSON.parse(xhr.responseText);
                        document.getElementById('text-notif-alertserti').innerText = responseData.message;
                        elementNotif.classList.remove('d-none');                        
                    }
                }
            });
        }
        else {
            document.getElementById('btn-cetakserti').classList.add('disabled');
        }
    });

    $("#form-cetaksertifikat").submit(function (event) {
        event.preventDefault();
    
        const baseUrlElement = document.getElementById('base-url');
        const baseUrl        = baseUrlElement.getAttribute('data-baseurl');

        var formData         = new FormData(this);

        Swal.fire({
            title: 'Cetak Sertifikat',
            text: "Sebelum mencetak sertifikat pastikan nama yang anda inputkan telah benar, cetak sertifikat ?",
            type: 'question',
            showCancelButton: true,
            cancelButtonColor: '#FF3D60',
            confirmButtonText: 'Ya, cetak'
        }).then(function (t){
            if (t.value) 
            {
                showLoading();
                $.ajax({
                    url: baseUrl + '/cetak-sertifikat',
                    type: "post",
                    data: formData,
                    dataType: 'json',
                    contentType: false,
                    cache: false,
                    processData: false,
                    success: function (response) {
                        if (response.statuslog == 'success') 
                        {
                            document.getElementById("form-cetaksertifikat").reset();
                            document.getElementById('text-notif-alertsertisuccess').innerHTML = response.message;
                            document.getElementById('notif-alertsertisuccess').classList.remove('d-none');
                            window.open(response.downloadlink, '_blank');
                        } else {
                            document.getElementById('text-notif-alertserti').innerText = response.message;
                            document.getElementById('notif-alertserti').classList.remove('d-none');
                        }                        
                        
                    },
                    complete: function(){
                        hideLoading();
                    },
                    error: function (xhr) {
                        var responseData = JSON.parse(xhr.responseText);
                        var message      = responseData.message;
                        // Disini harusnya bukan toastNotification
                        Swal.fire({
                            title: 'GAGAL',
                            text: message,
                            type: 'error',
                            confirmButtonColor: "#348cd4"
                        });
                    }
                });
            }
        })
                
    
    });



    // Swal.fire({
    // 	title: "Good job!",
    // 	text: "You clicked the button!",
    // 	type: "success",
    // 	confirmButtonColor: "#348cd4"
    // });
});