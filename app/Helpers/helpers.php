<?php

use App\Models\DataPendaftaran;
use App\Models\DataPeserta;
use Carbon\Carbon;

    if (!function_exists('generatePesertaID')) {
        function generatePesertaID($loop) {
            // Ambil nilai terakhir dari peserta_id dari database
            $lastPeserta = DataPendaftaran::orderBy('created_at', 'desc')->first();
        
            if ($lastPeserta) {
                // Jika ada data, ambil angka dari peserta_id terakhir
                $lastPesertaID = $lastPeserta->kode_invoice;
        
                // Ambil angka berikutnya dengan menambahkan 1
                $nextNumber = (int)substr($lastPesertaID, -4) + $loop;
        
                // Generate peserta_id baru dengan angka berikutnya
                // pmki-0001-1034
                $newPesertaID = 'idai-' . date('Hi') . '-' . str_pad($nextNumber, 4, '0', STR_PAD_LEFT) ;
            } else {
                // Jika tidak ada data, mulai dengan angka 0000
                // patklin-participant-0001
                $newPesertaID = 'idai-' . date('Hi') . '-0001';
            }
        
            return $newPesertaID;
        }
        
    }

    if (!function_exists('generateID')) {
        function generateID($loop) {
            // Ambil nilai terakhir dari peserta_id dari database
            $lastPeserta = DataPeserta::orderBy('pst_id', 'desc')->first();
        
            if ($lastPeserta) {
                // Jika ada data, ambil angka dari peserta_id terakhir
                $lastPesertaID = $lastPeserta->pst_id;
        
                // Ambil angka berikutnya dengan menambahkan 1
                $nextNumber = (int)substr($lastPesertaID, -4) + $loop;
        
                // Generate peserta_id baru dengan angka berikutnya
                // pmki-0001-1034
                $newPesertaID = 'ripiu-' . str_pad($nextNumber, 4, '0', STR_PAD_LEFT) ;
            } else {
                // Jika tidak ada data, mulai dengan angka 0000
                // patklin-participant-0001
                $newPesertaID = 'ripiu-' . '0001';
            }
        
            return $newPesertaID;
        }
        
    }

    if (!function_exists('statusPendaftaran')) {
        function statusPendaftaran($status)
        {
            if ($status == 'menunggu') {
                return '<span class="p-2 badge badge-primary">
                            <i>Menunggu Konfirmasi</i>
                        </span>';
            }
            elseif ($status == 'terverifikasi') {
                return '<span class="p-2 badge badge-success">
                            <i>Terkonfirmasi</i>
                        </span>';
            }
            elseif ($status == 'ditolak') {
                return '<span class="p-2 badge badge-danger">
                            <i>Ditolak</i>
                        </span>';
            }
            else{
                return 'Status Tidak Diketahui';
            }
        }
    }

    if (!function_exists('statusNotifikasi')) {
        function statusNotifikasi($status)
        {
            if ($status == 'berhasil') {
                return '<span class="p-2 badge badge-success">
                            <i>Berhasil</i>
                        </span>';
            }
            elseif ($status == 'gagal') {
                return '<span class="p-2 badge badge-danger">
                            <i>Gagal</i>
                        </span>';
            }
            else{
                return '<span class="p-2 badge badge-secondary">
                            <i>-</i>
                        </span>';
            }
        }
    }

    if (!function_exists('convertDate')) {
        function convertDate($value)
        {
            // Memisahkan teks menjadi dua tanggal
            $tanggalArray = explode(' - ', $value);
            
            // Mendapatkan tanggal awal dan tanggal akhir
            $tanggalAwal    = \Carbon\Carbon::createFromFormat('m/d/Y', $tanggalArray[0])->toDateString();
            $tanggalAkhir   = \Carbon\Carbon::createFromFormat('m/d/Y', $tanggalArray[1])->toDateString();
            // $jumlahHari     = $tanggalAwal->diffInDays($tanggalAkhir);

            $tanggalCheckin  = Carbon::parse($tanggalAwal);
            $tanggalCheckout = Carbon::parse($tanggalAkhir);

            // Hitung selisih hari
            $selisihHari = $tanggalCheckin->diffInDays($tanggalCheckout);

            
            return [
                'checkin'    => $tanggalAwal,
                'checkout'   => $tanggalAkhir,
                'jumlahhari' => $selisihHari,
            ];
        }
    }

    if (!function_exists('jumlahHari')) {
        function jumlahHari($tanggalAwal, $tanggalAkhir)
        {
            $tanggalCheckin  = Carbon::parse($tanggalAwal);
            $tanggalCheckout = Carbon::parse($tanggalAkhir);

            // Hitung selisih hari
            $selisihHari = $tanggalCheckin->diffInDays($tanggalCheckout);
            return $selisihHari;
        }
    }

    if (!function_exists('formatTotalBiaya')) {
        function formatTotalBiaya($totalbiaya, $kodeuniq)
        {
            $formattedTotalBiaya = number_format($totalbiaya, 0, ',', '.');
            $formattedTotalBiaya = substr($formattedTotalBiaya, 0, -3);
            $output              = $formattedTotalBiaya . '<span style="color:red;">' . $kodeuniq . '</span>,-';

            // Gabungkan format yang diinginkan
            $output = "Rp {$output}";

            return $output;
        }
    }
    
    if (!function_exists('convertTextToArray')) {
        function convertTextToArray($text) 
        {
            $text = strtolower($text);
            $text = str_replace(' + ', ' ', $text);
            $array = preg_split('/\s+/', $text);
            
            $result = [];
            foreach ($array as $item) {
                if (is_numeric($item)) {
                    $result[count($result) - 1] .= $item; 
                } else {
                    $result[] = $item;
                }
            }

            return $result;
        }
    }