<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Laravel\Sanctum\HasApiTokens;

class DataUsers extends Authenticatable
{
    use HasApiTokens, HasFactory, Notifiable;
    protected $table      = 'data_users'; // Nama tabel yang digunakan oleh model
    protected $primaryKey = 'userid'; // Nama kolom primary key
    protected $keyType    = 'string'; // Tipe data primary key
    public $incrementing  = false; // Set false jika menggunakan UUID

    protected $fillable = [
        'userid',
        'email',
        'password',
        'nama_lengkap',
        'no_hp',
        'level_akun',
        'status_akun',
        'terakhir_login',
    ];

    protected $dates = ['terakhir_login', 'created_at', 'updated_at'];
}
