<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class DataPaket extends Model
{
    use HasFactory;

    protected $table      = 'data_paketharga'; // Nama tabel yang digunakan oleh model
    protected $primaryKey = 'pketid'; // Nama kolom primary key
    protected $keyType    = 'string'; // Tipe data primary key
    public $incrementing  = false; // Set false jika menggunakan UUID

    protected $fillable = [
        'pketid',
        'jenis_peserta',
        'nama_paket',
        'harga_paket',
        'waktu',
        'status',
    ];

    protected $dates = ['waktu', 'created_at', 'updated_at'];
}
