<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class DataPeserta extends Model
{
    use HasFactory;

    protected $table      = 'data_peserta';
    protected $primaryKey = 'pst_id';
    public $incrementing  = false;
    protected $keyType    = 'string';

    protected $fillable = [
        'pst_id',
        'pdf_id',
        'paket_id',
        'kode_invoice',
        'jenis_peserta',
        'nama_paket',
        'harga_paket',
        'gelar_depan',
        'gelar_belakang',
        'nama_lengkap',
        'nik',
        'email',
        'nohp',
        'instansi_asal',
    ];

    protected $dates = ['created_at', 'updated_at'];

    public function pendaftaran()
    {
        return $this->belongsTo(DataPendaftaran::class, 'kode_invoice', 'kode_invoice');
    }

}
