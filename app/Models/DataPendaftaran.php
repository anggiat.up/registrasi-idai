<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class DataPendaftaran extends Model
{
    use HasFactory;

    protected $table      = 'data_pendaftaran'; // Nama tabel yang digunakan oleh model
    protected $primaryKey = 'pdftrid'; // Nama kolom primary key
    protected $keyType    = 'string'; // Tipe data primary key
    public $incrementing  = false; // Set false jika menggunakan UUID

    protected $fillable = [
        'pdftrid',
        'kode_invoice',
        'data_pendaftaran',
        'total_biaya',
        'kode_unik',
        'status_pembayaran',
        'bukti_pembayaran',
        'admin_verifikasi',
    ];

    protected $dates = ['created_at', 'updated_at'];
    protected $casts = [
        'data_pendaftaran' => 'array',
    ];

    public function admin()
    {
        return $this->belongsTo(DataUsers::class, 'admin_verifikasi', 'userid');
    }
}
