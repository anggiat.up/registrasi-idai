<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class AuthsController extends Controller
{
    public function index()
    {
        if (Auth::check()) {
            return redirect('sistem/dashboard');
        }

        $page = [
            'nama_halaman' => 'Administrasi | RiPiU3 2024',
            'appname'      => config('app.name')

        ];
        
        return view('layout/login-page', $page);
    }

    public function AjaxprosesLogin(Request $request)
    {
        $request->validate([
            'email'    => ['required','email'],
            'password' => 'required'
        ]);

        $credentials =    [
            'email'    => $request->email,
            'password' => $request->password
        ];
      

        if (Auth::attempt($credentials)) {
            
            // Jika login berhasil
            if (auth()->user()->status_akun === 'aktif') {
                return response()->json(['statuslog' => 'success', 'message' => 'Login Berhasil', 'title' => 'BERHASIL', 'links' => 'dashboard']);
            } else {
                // Status akun tidak aktif
                Auth::logout(); // Logout pengguna yang status akunnya tidak aktif
                return response()->json(['statuslog' => 'error', 'message' => 'Akun Anda tidak aktif. Hubungi administrator', 'title' => 'GAGAL',], 403);
            }
        }

        // Jika login gagal
        return response()->json(['statuslog'=> 'error' ,'message' => 'Email atau Password salah', 'title' => 'GAGAL',], 404);

        // Jika otentikasi gagal
        // return back()->with('loginError', 'Login Gagal');
    }

    public function logout(Request $request)
    {
        Auth::logout(); // Use Auth::logout() to log out the authenticated user
        $request->session()->invalidate();

        return redirect('/sistem/login');
    }
}
