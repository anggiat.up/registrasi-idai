<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class RegistrasiIdaiController extends Controller
{
    public function pageDashboard()
    {
        $page = [
            'nama_halaman' => 'Home',
            'appname'      => config('app.name')

        ];
       
        // return view('public-page/page-landing', $page);
        return view('public-pages/dashboard', $page);
    }

    public function pageKontak()
    {
        $page = [
            'nama_halaman' => 'Kontak Kami',
            'appname'      => config('app.name')

        ];
       
        // return view('public-page/page-landing', $page);
        return view('public-pages/kontak-kami', $page);
    }

    public function pageRegistrasi()
    {
        $page = [
            'nama_halaman' => 'Registrasi',
            'appname'      => config('app.name')

        ];
       
        // return view('public-page/page-landing', $page);
        return view('public-pages/registrasi-kegiatan', $page);
    }

    public function pageSertifikat()
    {
        $page = [
            'nama_halaman' => 'Cetak Sertifikat',
            'appname'      => config('app.name')

        ];
       
        // return view('public-page/page-landing', $page);
        return view('public-pages/sertifikat-kegiatan', $page);
    }
}
