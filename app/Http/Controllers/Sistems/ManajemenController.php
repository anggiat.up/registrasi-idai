<?php

namespace App\Http\Controllers\Sistems;

use App\Http\Controllers\Controller;
use App\Mail\NotificationEmail;
use App\Mail\NotificationPendaftaranPeserta;
use App\Models\DataPaket;
use App\Models\DataPendaftaran;
use App\Models\DataPeserta;
use App\Models\DataUsers;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Mail;
use Ramsey\Uuid\Uuid;
use Mpdf\Mpdf;
use Illuminate\Support\Facades\Storage;

use Intervention\Image\ImageManager;
use Intervention\Image\Drivers\Imagick\Driver;
use Intervention\Image\Typography\FontFactory;
use ZipArchive;



class ManajemenController extends Controller
{
    public function pgeDashboard()
    {
        $jumlah1      = DataPendaftaran::count();
        $jumlah2      = DataPendaftaran::where('status_pembayaran', 'menunggu')->count();
        $jumlah3      = DataPeserta::count();
        $jumlah4      = DataPendaftaran::where('status_pembayaran', 'terverifikasi')->count();
        $jumlah5      = DataPendaftaran::where('status_pembayaran', 'ditolak')->count();

        $mahakoas = DataPendaftaran::whereJsonContains('data_pendaftaran', [['jenis_peserta' => 'Mahasiswa / Ko-Asisten']])->count();
        $mahakoasverif = DataPendaftaran::where('status_pembayaran', 'terverifikasi')
                    ->whereJsonContains('data_pendaftaran', [['jenis_peserta' => 'Mahasiswa / Ko-Asisten']])
                    ->count();
        $umumppds = DataPendaftaran::whereJsonContains('data_pendaftaran', [['jenis_peserta' => 'Dokter Umum /PPDS']])->count();
        $umumppdsverif = DataPendaftaran::where('status_pembayaran', 'terverifikasi')
                    ->whereJsonContains('data_pendaftaran', [['jenis_peserta' => 'Dokter Umum /PPDS']])
                    ->count();
        $spesialis = DataPendaftaran::whereJsonContains('data_pendaftaran', [['jenis_peserta' => 'Dokter Spesialis']])->count();
        $spesialisverif = DataPendaftaran::where('status_pembayaran', 'terverifikasi')
                    ->whereJsonContains('data_pendaftaran', [['jenis_peserta' => 'Dokter Spesialis']])
                    ->count();

        $symposium = DataPeserta::where('nama_paket', 'Symposium')->count();
        $ws1       = DataPeserta::where('nama_paket', 'Symposium + Workshop 1')->count();
        $ws2       = DataPeserta::where('nama_paket', 'Symposium + Workshop 2')->count();
        $ws3       = DataPeserta::where('nama_paket', 'Symposium + Workshop 3')->count();
        $ws4       = DataPeserta::where('nama_paket', 'Symposium + Workshop 4')->count();
        
        $page = [
            'nama_halaman'  => 'Manajemen',
            'appname'       => config('app.name'),
            'jumlah1'       => $jumlah1,
            'jumlah2'       => $jumlah2,
            'jumlah3'       => $jumlah3,
            'jumlah4'       => $jumlah4,
            'jumlah5'       => $jumlah5,
            'mahakoas'      => $mahakoas,
            'mahakoasverif' => $mahakoasverif,
            'umumppds'      => $umumppds,
            'umumppdsverif' => $umumppdsverif,
            'spesialis'      => $spesialis,
            'spesialisverif' => $spesialisverif,
            'symposium'      => $symposium,
            'ws1'      => $ws1,
            'ws2'      => $ws2,
            'ws3'      => $ws3,
            'ws4'      => $ws4,
            
            // 'kode_invoice' => 'IVC123',
            // 'total_biaya'  => 21750000,
            // 'kode_unik'    => 678,
            // 'nama_lengkap' => 'Andre',
            // 'nama_paket'   => 'Workshop + Symposium',

            // 'pst_id' => 'PMKI-0012'
        ];

        // echo auth()->user()->nama_lengkap;
        return view('admin-pages/stm-dashboard', $page);
        // return view('layout/mail-notifikasipembayaran', $page);
        // return view('layout/mail-notifikasipendaftaran', $page);
    }

    public function pgePaketKegiatan()
    {
        $page = [
            'nama_halaman' => 'Paket Kegiatan',
            'appname'      => config('app.name')

        ];

        // echo auth()->user()->nama_lengkap;
        return view('admin-pages/stm-paketkegiatan', $page);
    }

    public function pgePendaftaranPeserta()
    {
        $page = [
            'nama_halaman' => 'Pendaftaran Peserta',
            'appname'      => config('app.name')

        ];
        // return view('layout/mail-notifikasipembayaran', $page);
        // die;
        
        // echo auth()->user()->nama_lengkap;
        return view('admin-pages/stm-datapendaftaran', $page);  

    }

    public function pgePesertaTerverifikasi()
    {
        $page = [
            'nama_halaman' => 'Data Peserta',
            'appname'      => config('app.name')

        ];
        // return view('layout/mail-notifikasipembayaran', $page);
        // die;
        
        // echo auth()->user()->nama_lengkap;
        return view('admin-pages/stm-datapeserta', $page);

    }

    public function pgeDataUser()
    {
        $page = [
            'nama_halaman' => 'Data User',
            'appname'      => config('app.name')

        ];
        
        // echo auth()->user()->nama_lengkap;
        return view('admin-pages/stm-datauser', $page);

    }

    public function template()
    {
        $page = [
            'pst_id' => 'PS021',
            'nama_lengkap' => 'Maxim',
            'nama_paket' => 'Symposium',
            'kode_invoice' => 'IDAI123123',
            'total_biaya' => 1000000,
            'kode_unik' => 345,

        ];
        return view('layout/mail-notifikasipembayaran', $page);
        // return view('layout/mail-notifikasipendaftaran', $page);
    }

    public function reqCetakLaporan(Request $request)
    {
        $jenis       = $request->jenis;
        $dataPeserta = NULL;

        if ($jenis == 'semua') {
            $dataPeserta = DataPeserta::orderBy('pst_id', 'asc')->get();
        } 
        elseif ($jenis == 'symposium') {
            $dataPeserta = DataPeserta::where('nama_paket', 'Symposium')->orderBy('pst_id', 'asc')->get();
        } 
        elseif ($jenis == 'workshop1') {
            $dataPeserta = DataPeserta::where('nama_paket', 'Symposium + Workshop 1')->orderBy('pst_id', 'asc')->get();
        } 
        elseif ($jenis == 'workshop2') {
            $dataPeserta = DataPeserta::where('nama_paket', 'Symposium + Workshop 2')->orderBy('pst_id', 'asc')->get();
        } 
        elseif ($jenis == 'workshop3') {
            $dataPeserta = DataPeserta::where('nama_paket', 'Symposium + Workshop 3')->orderBy('pst_id', 'asc')->get();
        } 
        elseif ($jenis == 'workshop4') {
            $dataPeserta = DataPeserta::where('nama_paket', 'Symposium + Workshop 4')->orderBy('pst_id', 'asc')->get();
        } 
        
        $page = [
            'nama_halaman' => 'Cetak Laporan',
            'appname'      => config('app.name'),
            'datapeserta'  => $dataPeserta ,
        ];
            
        $mpdf = new Mpdf([
                        'mode'          => 'utf-8',
                        'format'        => 'A4-L', // A4 landscape
                        'margin_left'   => 5, // 2,54 cm = 5 mm
                        'margin_right'  => 5, // 2,54 cm = 5 mm
                        'margin_top'    => 5, // 2,54 cm = 5 mm
                        'margin_bottom' => 5, // 2,54 cm = 5 mm
                        'margin_header' => 0,
                        'margin_footer' =>  0,
        ]);
        
        
        $mpdf->WriteHTML(view('admin-pages/stm-cetaklaporan', $page));
        $mpdf->Output();
        // echo auth()->user()->nama_lengkap;
        // return view('admin-pages/stm-cetaklaporan', $page);
    }














    // =======================================
    // Req Ajax




    public function reqAjaxTablePendaftaranPeserta(Request $request)
    {
        if (request()->ajax()) 
        {
            return DataTables()->of(DataPendaftaran::whereIn('status_pembayaran', ['menunggu', 'ditolak'])->orderBy('created_at', 'desc')->get())
                ->addIndexColumn()
                ->editColumn('kode_invoice', function($row){
                    return strtoupper($row->kode_invoice);
                })
                ->editColumn('status_pembayaran', function($row){
                    return statusPendaftaran($row->status_pembayaran);
                })
                ->editColumn('total_biaya', function($row){
                    return 'Rp. ' . number_format($row->total_biaya) . '.-';
                })
                ->editColumn('created_at', function($row){
                    return date('H:i/d-M-Y', strtotime($row->created_at));
                })
                ->editColumn('notifikasi', function($row){
                    return statusNotifikasi($row->notifikasi_pembayaran) . ' | ' . statusNotifikasi($row->notifikasi_pendaftaran);
                })
                ->editColumn('file_bukti', function($row){
                    if ($row->bukti_pembayaran == null) {
                        return 'Belum Upload';
                    }
                    else{
                        return '<a href="javascript:void(0);" class="" onclick="filePembayaran(\''. $row->pdftrid .'\')">
                                    <i class="fas fa-file-invoice-dollar btn btn-sm btn-secondary p-2"></i>
                                </a>';
                    }
                })
                ->editColumn('aksi', function($row){

                    $aksi = '<a href="javascript:void(0);" class="" onclick="detailPendaftaran(\''. $row->pdftrid .'\')">
                                <i class="fas fa-eye btn btn-sm btn-primary p-2"></i>
                            </a>';

                    if ($row->status_pembayaran == 'menunggu') {
                        $aksi .=    '<a href="javascript:void(0);" class="" onclick="konfirmasiPendaftaran(\''. $row->pdftrid .'\')">
                                        <i class="fas fa-check btn btn-sm btn-success p-2"></i>
                                    </a>
                                    <a href="javascript:void(0);" class="" onclick="tolakPendaftaran(\''. $row->pdftrid .'\')">
                                        <i class="fas fa-times btn btn-sm btn-danger p-2"></i>
                                    </a>';
                        
                        if (auth()->user()->level_akun == 'admin') {
                            $aksi .= '<a href="javascript:void(0);" class="" onclick="hapusPendaftaran(\''. $row->pdftrid .'\')">
                                        <i class="fas fa-trash btn btn-sm btn-dark p-2"></i>
                                    </a>';
                        }
                        
                        return $aksi;
                    }
                    else{
                        if (auth()->user()->level_akun == 'admin') {
                            $aksi .= '<a href="javascript:void(0);" class="" onclick="hapusPendaftaran(\''. $row->pdftrid .'\')">
                                        <i class="fas fa-trash btn btn-sm btn-dark p-2"></i>
                                    </a>';
                        }
                        return $aksi;
                    }
                })
                ->rawColumns(['aksi', 'file_bukti', 'status_pembayaran', 'notifikasi'])
                ->make(true);
        }
    }

    public function reqAjaxTablePeserta(Request $request)
    {
        if (request()->ajax()) 
        {
            return DataTables()->of(DataPeserta::orderBy('pst_id', 'desc')->get())
                ->addIndexColumn()
                ->editColumn('pst_id', function($row){
                    return strtoupper($row->pst_id);
                })
                ->editColumn('kode_invoice', function($row){
                    return strtoupper($row->kode_invoice);
                })
                ->editColumn('nama_lengkap', function($row){
                    return $row->gelar_depan . ' ' . $row->nama_lengkap . ' ' . $row->gelar_belakang;
                })
                
                ->editColumn('created_at', function($row){
                    return date('H:i/d-M-Y', strtotime($row->created_at));
                })                
                ->editColumn('notif', function($row){
                    // return statusNotifikasi($row->notifikasi_pembayaran) . ' | ' . statusNotifikasi($row->notifikasi_pendaftaran);
                    return statusNotifikasi($row->pendaftaran->notifikasi_pembayaran) . ' | ' . statusNotifikasi($row->pendaftaran->notifikasi_pendaftaran);
                })
                ->editColumn('aksi', function($row){

                    $aksi = '<a href="javascript:void(0);" class="" onclick="filePembayaran(\''. $row->pendaftaran->pdftrid .'\')">
                                <i class="fas fa-file-invoice-dollar btn btn-sm btn-secondary p-2"></i>
                            </a>
                            <a href="javascript:void(0);" class="" onclick="detailPendaftaran(\''. $row->pendaftaran->pdftrid .'\')">
                                <i class="fas fa-eye btn btn-sm btn-primary p-2"></i>
                            </a>';
                  
                    return $aksi;                  
                })
                ->rawColumns(['aksi','notif'])
                ->make(true);
        }
    }

    public function reqAjaxTableDataPaket(Request $request)
    {
        if (request()->ajax()) 
        {
            return DataTables()->of(DataPaket::orderBy('jenis_peserta', 'asc')->orderBy('nama_paket', 'asc')->get())
                ->addIndexColumn()
                ->editColumn('harga', function($row){
                    return 'Rp ' . number_format($row->harga_paket) . '.-';
                })
                ->editColumn('aksi', function($row){
                    return '<a href="javascript:void(0);" class="" onclick="editDataPaket(\''. $row->pketid .'\')">
                                <i class="fas fa-pen btn btn-sm btn-warning p-2"></i>
                            </a>';
                })
                ->rawColumns(['aksi'])
                ->make(true);
        }
    }

    public function reqAjaxHargaPaket(Request $request)
    {
        if (request()->ajax()) 
        {
            $cari            = ['jenis_peserta' => $request->peserta, 'status' => 'Active'];
            $data            = DataPaket::where($cari)->orderBy('nama_paket', 'asc')->get()->toArray();
            $result          = NULL;

            if ($data) 
            {
                $result = '<option value="" hidden selected disabled>-- Jenis Kegiatan --</option>';
                foreach ($data as $key => $value) {
                    if ($value['nama_paket'] == 'Workshop + Symposium') {
                        $result .= '<option value="' . $value['pketid'] . '" data-harga="'.$value['harga_paket'].'">' . $value['nama_paket'] . ' | Rp. '. number_format($value['harga_paket']) .' Have get Disc 10%</option>';
                    }
                    else{
                        $result .= '<option value="' . $value['pketid'] . '" data-harga="'.$value['harga_paket'].'">' . $value['nama_paket'] . ' | Rp. '. number_format($value['harga_paket']) .' </option>';
                    }
                }                
                return response()->json($result);
            }
            else{
                return response()->json(['statuslog'=> 'error' ,'message' => 'Data tidak ditemukan', 'title'=> 'GAGAL'], 404);
            }
        }
    }

    public function reqAjaxRegistrasiPeserta(Request $request)
    {
        if (request()->ajax()) 
        {
            $jenis_partisipasi    = $request->input('jenis-partisipasi');
            $jenis_kegiatan       = $request->input('jenis-kegiatan');
            $total_biaya          = $request->input('total-biaya');
            $gelar_depan          = $request->input('gelar-depan');
            $gelar_belakang       = $request->input('gelar-belakang');
            $nama_lengkap_peserta = $request->input('nama-lengkap-peserta');
            $nik                  = $request->input('nik');
            $email                = $request->input('email');
            $nohp                 = $request->input('nohp');
            $instansi_asal        = $request->input('instansi-asal');

            $data            = DataPaket::where(['pketid' => $jenis_kegiatan])->first()->toArray();
            $harga_paket     = $data['harga_paket'];
            $nama_paket      = $data['nama_paket'];
            $kode_uniq       = mt_rand(100, 999);
            $invoice         = generatePesertaID(1);
            $pdftrid         = Uuid::uuid4();

            $data_peserta[] = [
                                    'jenis_peserta'  => $jenis_partisipasi,
                                    'paket_id'       => $jenis_kegiatan,
                                    'nama_paket'     => $nama_paket,
                                    'harga_paket'    => $harga_paket,
                                    'gelar_depan'    => $gelar_depan,
                                    'gelar_belakang' => $gelar_belakang,
                                    'nama_lengkap'   => $nama_lengkap_peserta,
                                    'nik'            => $nik,
                                    'email'          => $email,
                                    'nohp'           => $nohp,
                                    'instansi_asal'  => $instansi_asal,
                                ];
            $data_registrasi = [
                                    'pdftrid'           => $pdftrid,
                                    'kode_invoice'      => $invoice,
                                    'data_pendaftaran'  => json_encode($data_peserta),
                                    'total_biaya'       => $harga_paket + $kode_uniq,
                                    'kode_unik'         => $kode_uniq,
                                    'status_pembayaran' => 'menunggu',
                                    'bukti_pembayaran'  => NULL,
                                    'admin_verifikasi'  => NULL,
                                    'created_at'        => now(),
                                    'updated_at'        => now()
                                ];
            
            $data_email      = [
                                    'nama_lengkap' => $gelar_depan . ' ' . $nama_lengkap_peserta . ' ' . $gelar_belakang,
                                    'email'        => $email,
                                    'kode_invoice' => $invoice,
                                    'nama_paket'   => $nama_paket,
                                    'total_biaya'  => $harga_paket,
                                    'kode_unik'    => $kode_uniq,
            ];
                                
            $datapesertapdf = DataPendaftaran::whereJsonContains('data_pendaftaran', ['email' => $email])->first();
            if (!$datapesertapdf) 
            {
                try {
                    DB::beginTransaction();
                        DataPendaftaran::insert($data_registrasi);
                    DB::commit();

                    try {


                        Mail::to($email)->send(new NotificationEmail($data_email));
                        DataPendaftaran::where(['pdftrid' => $pdftrid])->update(['notifikasi_pembayaran' => 'berhasil']);
                        return response()->json(['statuslog' => 'success', 'message' => 'Registrasi berhasil silahkan check email anda untuk melanjutkan proses pembayaran', 'title' => 'BERHASIL']);
    
                    } catch (\Throwable $th) {
                        // return response()->json(['statuslog' => 'success', 'message' =>  $th->getMessage() , 'title' => 'BERHASIL']);
                        DataPendaftaran::where(['pdftrid' => $pdftrid])->update(['notifikasi_pembayaran' => 'gagal']);
                        return response()->json(['statuslog' => 'success', 'message' =>  'Registrasi berhasil namun jaringan tidak stabil sehingga pengiriman email gagal silahkan klik menu notifikasi ulang' , 'title' => 'BERHASIL']);
                    }
                } 
                catch (\Exception $e) 
                {
                    DB::rollback();
                    return response()->json(['statuslog' => 'error', 'message' => 'Registrasi gagal, mohon coba kembali', 'title' => 'GAGAL'], 404);
                }
            }
            else{
                return response()->json(['statuslog' => 'error', 'message' => 'Alamat Email yang anda gunakan telah terdaftar didalam sistem, jika anda belum menerima email silahkan lakukan notifikasi ulang email'], 404);
            }
            
        }
        else{
            return response()->json(['statuslog'=> 'error' ,'message' => 'Gagal Mengakses Halaman'], 404);
        }
    }

    public function reqAjaxInvoice(Request $request)
    {
        if (request()->ajax()) 
        {
            $cari            = $request->invoice;
            $data            = DataPendaftaran::where('kode_invoice', $cari)->first();
            $result          = NULL;

            if ($data) 
            {   
                $data = $data->toArray(); 
                $result  = [
                                'nama_lengkap' => $data['data_pendaftaran'][0]['gelar_depan'] . ' ' . $data['data_pendaftaran'][0]['nama_lengkap'] . ' ' . $data['data_pendaftaran'][0]['gelar_belakang'],                                
                                'nama_paket'   => $data['data_pendaftaran'][0]['nama_paket'],
                                'total_biaya'  => 'Rp. ' . number_format($data['total_biaya']),
                            ];
                                
                return response()->json($result);
            }
            else{
                return response()->json(['statuslog'=> 'error' ,'message' => 'Data tidak ditemukan', 'title'=> 'GAGAL'], 404);
            }
        }
    }

    public function reqAjaxPstSertifikat(Request $request)
    {
        if (request()->ajax()) 
        {
            $cari            = strtolower($request->idpesertaripiu);
            $data            = DataPeserta::where('pst_id', $cari)->first();
            $result          = NULL;

            if (strlen($request->idpesertaripiu) > 10) {
                return response()->json(['statuslog'=> 'error' ,'message' => 'Harap inputkan 10 digit nomor peserta anda, cth : RIPIU-0999', 'title'=> 'GAGAL'], 404);
            }

            if ($data) {   
                $data = $data->toArray(); 
                $result  = [
                                'nama_lengkap' => $data['gelar_depan'] . ' ' . $data['nama_lengkap'] . ' ' . $data['gelar_belakang'],                                
                                'nama_paket'   => $data['nama_paket'],
                            ];
                                
                return response()->json($result);
            }
            else{
                return response()->json(['statuslog'=> 'error' ,'message' => 'Data tidak ditemukan', 'title'=> 'GAGAL'], 404);
            }
        }
    }

    public function reqAjaxCetakSertifikat(Request $request)
    {
        if (request()->ajax()) 
        {
            $cari              = strtolower($request->idpesertaripiu);
            $namaBaruPeserta   = $request->namalengkappeserta;
            $data              = DataPeserta::where('pst_id', $cari)->first();
            $result            = NULL;

            if ($data) {   
                $dataNew         = $data->toArray(); 
                $arsipSertifikat = NULL; 

                $genSertifikat = convertTextToArray($dataNew['nama_paket']);
                foreach ($genSertifikat as $value) 
                {
                    if ($this->generateCertificate($namaBaruPeserta, $dataNew, $value . '.jpg', $value)) {
                        $arsipSertifikat[] = strtolower($dataNew['pst_id']) . '-'. $value .'.jpg';
                    }
                }
                
                if (!empty($arsipSertifikat)) { 

                    $zipFilePath = public_path('generate-sertifikat/' . strtolower($dataNew['pst_id']) . '-sertifikat.zip');
                    $zip = new ZipArchive();

                    if ($zip->open($zipFilePath, ZipArchive::CREATE) !== TRUE) {
                        return response()->json(['statuslog'=> 'error' ,'message' => 'Tidak bisa melakukan arsip sertifikat', 'title'=> 'GAGAL'], 404);
                    }

                    foreach ($arsipSertifikat as $file) 
                    {
                        if (file_exists(public_path('generate-sertifikat/' . $file))) {
                            $zip->addFile(public_path('generate-sertifikat/' . $file), basename($file));
                        }
                    }
                    $zip->close();

                    if (file_exists($zipFilePath)) {
                        return response()->json([
                            'statuslog'     => 'success',
                            'message'       => 'Sertifikat berhasil digenerate. Jika proses download tidak berjalan silahkan klik link berikut. <a href="'. url('download/' . strtolower($dataNew['pst_id']) . '-sertifikat.zip').'" target="_blank">DOWNLOAD</a>',
                            'title'         => 'BERHASIL',
                            'downloadlink'  => url('download/' . strtolower($dataNew['pst_id']) . '-sertifikat.zip')
                        ], 200);
                    } else {
                        return response()->json(['statuslog'=> 'error' ,'message' => 'Gagal membuat file ZIP.', 'title'=> 'GAGAL'], 500);
                    }
                }
                else{
                    return response()->json(['statuslog'=> 'error' ,'message' => 'Proses generate Sertifikat gagal', 'title'=> 'GAGAL'], 404);
                }

            }
            else{
                return response()->json(['statuslog'=> 'error' ,'message' => 'Data tidak ditemukan', 'title'=> 'GAGAL'], 404);
            }
        }
    }

    public function generateCertificate($namaBaruPeserta, $dataNew, $namaTmpt, $namasertifikat)
    {
        $templatePath = 'template-sertifikat/' . $namaTmpt;
        
        $image = ImageManager::imagick()->read(public_path($templatePath));

        $imageWidth = $image->width() / 2;
        $imageHeight = 855;

        // Menambahkan teks ke gambar
        $image->text($namaBaruPeserta, $imageWidth, $imageHeight, function (FontFactory $font) {
            $font->filename(public_path('template-sertifikat/Roboto-Medium.ttf'));
            $font->size(95);
            $font->color('000');
            $font->align('center');
            $font->valign('middle');
            $font->lineHeight(1.6);
            $font->angle(0);
        });

        $filename = $dataNew['pst_id'] . '-'. $namasertifikat .'.jpg';
        $savePath = public_path('generate-sertifikat/' . $filename);

        try {
            $image->save($savePath);
            return true;
        } catch (\Exception $e) {
            return false;
        }

    }

    public function reqAjaxFormUploadPembayaran(Request $request)
    {
        if (request()->ajax()) 
        {
            $cari            = $request->kode_invoice;
            $data            = DataPendaftaran::where('kode_invoice', $cari)->first();
            $file            = $request->file('bukti-pembayaran');

            if ($file->getSize() > 1048576) {
                return response()->json(['statuslog' => 'error', 'message' => 'Ukuran file terlalu besar. Maksimum 1 MB', 'title' => 'GAGAL'], 404);
            }

            $allowedExtensions = ['jpg', 'jpeg', 'png', 'gif'];
            if (!in_array(strtolower($file->getClientOriginalExtension()), $allowedExtensions)) {
                return response()->json(['statuslog' => 'error', 'message' => 'Jenis file tidak didukung. Hanya diperbolehkan: JPG, JPEG, PNG, GIF', 'title' => 'GAGAL'], 404);
            }

            $nama_file       = uniqid() . '-' . time() . '.' . $file->getClientOriginalExtension();
            $file->move(public_path('bukti-pembayaran'), $nama_file);

            
            $data_paket = [
                                    'bukti_pembayaran' => $nama_file,                         
                                    'updated_at'       => now()
                                ];
            try {
                DB::beginTransaction();
                    DataPendaftaran::where(['kode_invoice' => $cari])->update($data_paket);
                DB::commit();
                return response()->json(['statuslog' => 'success', 'message' => 'Bukti Pembayaran berhasil diupload', 'title' => 'BERHASIL']);
            } 
            catch (\Exception $e) 
            {
                DB::rollback();
                return response()->json(['statuslog' => 'error', 'message' => 'Gagal mengupload Bukti Pembayaran, mohon coba kembali', 'title' => 'GAGAL'], 404);
            }
        }
        else{
            return response()->json(['statuslog'=> 'error' ,'message' => 'Gagal Mengakses Halaman'], 404);
        }
    }

    public function reqAjaxDataPesertaPendaftaran(Request $request)
    {   
        if (request()->ajax()) 
        {
            $cari            = $request->pendaftaran;
            $data            = DataPendaftaran::with('admin')->where('pdftrid', $cari)->first();
            $result          = NULL;

            if ($data) 
            {   
                $data             = $data->toArray(); 
                $admin            = isset($data['admin']) ? $data['admin'] : null;
                $data_pendaftaran = $data['data_pendaftaran'];
                
                $result  = [
                                'kode_invoice'       => strtoupper($data['kode_invoice']),
                                'total_biaya'        => 'Rp. ' . number_format($data['total_biaya']),
                                'kode_unik'          => $data['kode_unik'],
                                'status_pembayaran'  => ucfirst($data['status_pembayaran']),
                                'admin_verifikasi'   => $admin ? $admin['nama_lengkap'] : '-',
                                'waktu_pendaftaran'  => date('H:i d-M-Y', strtotime($data['created_at'])),
                            ];
                
                $table_peserta = '<table class="table-sm" width="100%">';

                foreach ($data_pendaftaran as $value) {
                    $table_peserta .= '
                                        <tr>
                                            <th width="30%">Paket Kegiatan</th>
                                            <td width="5%">:</td>
                                            <td>'. $value["nama_paket"] .'</td>
                                        </tr>
                                        <tr>
                                            <th>Nama Lengkap Peserta</th>
                                            <td>:</td>
                                            <td> ' . $value["gelar_depan"] . ' ' . $value["nama_lengkap"] . ' ' . $value["gelar_belakang"] .' </td>
                                        </tr>
                                        <tr>
                                            <th>NIK</th>
                                            <td>:</td>
                                            <td>'. $value["nik"] .'</td>
                                        </tr>
                                        <tr>
                                            <th>Email</th>
                                            <td>:</td>
                                            <td>'. $value["email"] .'</td>
                                        </tr>
                                        <tr>
                                            <th>Nomor Hp</th>
                                            <td>:</td>
                                            <td>'. $value["nohp"] .'</td>
                                        </tr>
                                        <tr>
                                            <th>Instansi Asal</th>
                                            <td>:</td>
                                            <td>'. $value["instansi_asal"] .'</td>
                                        </tr>
                                        <tr>
                                            <td colspan="3"><hr></td>
                                        </tr>
                                    ';
                }

                $table_peserta .= ' </table>';

                $result['data_pendaftaran'] = $table_peserta;
                return response()->json($result);
            }
            else{
                return response()->json(['statuslog'=> 'error' ,'message' => 'Data tidak ditemukan', 'title'=> 'GAGAL'], 404);
            }
        }
    }

    public function reqAjaxBuktiPembayaran(Request $request)
    {
        if (request()->ajax()) 
        {
            $cari            = $request->pendaftaran;
            $data            = DataPendaftaran::where('pdftrid', $cari)->first();
            $result          = NULL;

            if ($data) 
            {   
                $data = $data->toArray(); 
                $result  = [
                                'kode_invoice'     => strtoupper($data['kode_invoice']),
                                'total_biaya'      => 'Rp. ' . number_format($data['total_biaya']),
                                'kode_unik'        => $data['kode_unik'],
                                'bukti_pembayaran' => asset('bukti-pembayaran/' . $data['bukti_pembayaran']),
                                'btn_verify'       => '-'
                            ];
                
                if ($data['status_pembayaran'] == 'menunggu') {
                    $result['btn_verify'] =     '<a href="javascript:void(0);" class="btn btn-sm btn-success" onclick="konfirmasiPendaftaran(\''. $cari .'\')">
                                                    <i class="fas fa-check  p-2"></i> Konfirmasi Pendaftaran
                                                </a>
                                                <a href="javascript:void(0);" class="btn btn-sm btn-danger" onclick="tolakPendaftaran(\''. $cari .'\')">
                                                    <i class="fas fa-times  p-2"></i> Pembayaran Tidak Ditemukan
                                                </a>';
                }
                                
                return response()->json($result);
            }
            else{
                return response()->json(['statuslog'=> 'error' ,'message' => 'Data tidak ditemukan', 'title'=> 'GAGAL'], 404);
            }
        }
    }

    public function reqAjaxVerifikasi(Request $request)
    {
        if (request()->ajax()) 
        {
            $pdftrid           = $request->pendaftaran;
            $status_pembayaran = $request->status;
            $admin_verifikasi  = auth()->user()->userid;

            $pendaftaran = [
                                    'status_pembayaran' => $status_pembayaran,
                                    'admin_verifikasi'  => $admin_verifikasi,                  
                                    'updated_at'        => now()
                                ];
            try {
                DB::beginTransaction();
                    DataPendaftaran::where(['pdftrid' => $pdftrid])->update($pendaftaran);
                DB::commit();

                $data             = DataPendaftaran::where('pdftrid', $pdftrid)->first();
                $data             = $data->toArray(); 
                $data_pendaftaran = $data['data_pendaftaran'];
                $nomor            = 0;
                $data_peserta     = [];

                foreach ($data_pendaftaran as $value) 
                {
                    $nomor += 1;
                    $data_peserta[] = [
                        'pst_id'         => generateID($nomor),
                        'pdf_id'         => $pdftrid,
                        'kode_invoice'   => $data['kode_invoice'],
                        'jenis_peserta'  => $value['jenis_peserta'],
                        'paket_id'       => $value['paket_id'],
                        'nama_paket'     => $value['nama_paket'],
                        'harga_paket'    => $value['harga_paket'],
                        'gelar_depan'    => $value['gelar_depan'],
                        'gelar_belakang' => $value['gelar_belakang'],
                        'nama_lengkap'   => $value['nama_lengkap'],
                        'nik'            => $value['nik'],
                        'email'          => $value['email'],
                        'nohp'           => $value['nohp'],
                        'instansi_asal'  => $value['instansi_asal'],
                        'created_at'     => now(),
                        'updated_at'     => now()
                    ];
                    
                }

                
                if ($status_pembayaran == 'terverifikasi') {
                    DB::beginTransaction();
                        DataPeserta::insert($data_peserta);
                    DB::commit();
                    
                    try {
                        Mail::to($data_peserta[0]['email'])->send(new NotificationPendaftaranPeserta($data_peserta[0]));
                        DataPendaftaran::where(['pdftrid' => $pdftrid])->update(['notifikasi_pendaftaran' => 'berhasil']);
                        return response()->json(['statuslog' => 'success', 'message' => 'Pendaftaran Peserta berhasil ' .  ucfirst($status_pembayaran) . ' dan notifikasi email berhasil dikirim', 'title' => 'BERHASIL']);
                    } 
                    catch (\Throwable $th) {
                        DataPendaftaran::where(['pdftrid' => $pdftrid])->update(['notifikasi_pendaftaran' => 'gagal']);
                        return response()->json(['statuslog' => 'success', 'message' => 'Pendaftaran Peserta berhasil ' .  ucfirst($status_pembayaran) . ' namun notifikasi email gagal dikirim.'  . $th->getMessage(), 'title' => 'BERHASIL']);
                    }
                }
                else{
                    return response()->json(['statuslog' => 'success', 'message' => 'Pendaftaran telah ' .  ucfirst($status_pembayaran) . ' Segera kontak langsung peserta untuk notifikasi lebih baik.' , 'title' => 'BERHASIL']);
                }
                
            } 
            catch (\Exception $e) 
            {
                DB::rollback();
                return response()->json(['statuslog' => 'error', 'message' => 'Gagal Memverifikasi Peserta, mohon coba kembali', 'title' => 'GAGAL'], 404);
            }
        }
        else{
            return response()->json(['statuslog'=> 'error' ,'message' => 'Gagal Mengakses Halaman'], 404);
        }
    }

    public function reqAjaxHapusPendaftaran(Request $request)
    {
        if (request()->ajax()) 
        {
            $pdftrid           = $request->pendaftaran;
            $status_pembayaran = $request->status;
            $admin_verifikasi  = auth()->user()->pmkid;

            try {
                DB::beginTransaction();
                    DataPendaftaran::where(['pdftrid' => $pdftrid])->delete();
                DB::commit();
                return response()->json(['statuslog' => 'success', 'message' => 'Pendaftaran Peserta berhasil dihapus', 'title' => 'BERHASIL']);
            } 
            catch (\Exception $e) 
            {
                DB::rollback();
                return response()->json(['statuslog' => 'error', 'message' => 'Gagal menghapus data Peserta, mohon coba kembali', 'title' => 'GAGAL'], 404);
            }
        }
        else{
            return response()->json(['statuslog'=> 'error' ,'message' => 'Gagal Mengakses Halaman'], 404);
        }
    }

    public function reqAjaxTableDataUser(Request $request)
    {
        if (request()->ajax()) 
        {
            return DataTables()->of(DataUsers::orderBy('nama_lengkap', 'asc')->get())
                ->addIndexColumn()
                ->editColumn('level_akun', function($row){
                    return ucfirst($row->level_akun);
                })
                ->editColumn('status_akun', function($row){
                    return $row->status_akun;
                })
                ->editColumn('aksi', function($row){
                    return '<a href="javascript:void(0);" class="" onclick="editDataUser(\''. $row->userid .'\')">
                                <i class="fas fa-pen btn btn-sm btn-warning p-2"></i>
                            </a>
                            <a href="javascript:void(0);" class="" onclick="hapusUser(\''. $row->pmkid .'\')">
                                <i class="fas fa-times btn btn-sm btn-danger p-2"></i>
                            </a>';
                })
                ->rawColumns(['aksi'])
                ->make(true);
        }
    }

    public function reqAjaxDataUser(Request $request)
    {
        if (request()->ajax()) 
        {
            $cari            = ['userid' => $request->user];
            $data            = DataUsers::where($cari)->first();
            $result          = NULL;

            if ($data) 
            {
                $result['userid']       = $data->userid;
                $result['email']        = $data->email;
                $result['nama_lengkap'] = $data->nama_lengkap;
                $result['no_hp']        = $data->no_hp;
                $result['level_akun']   = $data->level_akun;
                $result['status_akun']  = $data->status_akun;
                
                return response()->json($result);
            }
            else{
                return response()->json(['statuslog'=> 'error' ,'message' => 'Data tidak ditemukan', 'title'=> 'GAGAL'], 404);
            }
        }
    }

    public function reqAjaxFormTambahUser(Request $request)
    {
        if (request()->ajax()) 
        {
            $userid        = Uuid::uuid4();
            $nama_lengkap = $request->nama_lengkap;
            $email        = $request->email;
            $password     = $request->password;
            $no_hp        = $request->no_hp;
            $level_user   = $request->level_user;
            $status       = $request->status;

            $datausers = [
                            'userid'       => $userid,
                            'nama_lengkap' => $nama_lengkap,
                            'email'        => $email,
                            'password'     => bcrypt($password),
                            'no_hp'        => $no_hp,
                            'level_akun'   => $level_user,
                            'status_akun'  => $status,
                            'created_at'   => now(),
                            'updated_at'   => now()
                        ];
            try {
                DB::beginTransaction();
                DataUsers::insert($datausers);
                DB::commit();
                return response()->json(['statuslog' => 'success', 'message' => 'Data User baru berhasil disimpan', 'title' => 'BERHASIL']);
            } 
            catch (\Exception $e) 
            {
                DB::rollback();
                return response()->json(['statuslog' => 'error', 'message' => 'Gagal menambahkan Data User, mohon coba kembali', 'title' => 'GAGAL'], 404);
            }
        }
        else{
            return response()->json(['statuslog'=> 'error' ,'message' => 'Gagal Mengakses Halaman'], 404);
        }
    }

    public function reqAjaxDataPaket(Request $request)
    {
        if (request()->ajax()) 
        {
            $cari            = ['pketid' => $request->paket];
            $data            = DataPaket::where($cari)->first();
            $result          = NULL;

            if ($data) 
            {
                $result['pketid']        = $data->pketid;
                $result['asal_peserta']  = $data->asal_peserta;
                $result['jenis_peserta'] = $data->jenis_peserta;
                $result['nama_paket']    = $data->nama_paket;
                $result['harga_paket']   = $data->harga_paket;
                $result['status']        = $data->status;
                
                return response()->json($result);
            }
            else{
                return response()->json(['statuslog'=> 'error' ,'message' => 'Data tidak ditemukan', 'title'=> 'GAGAL'], 404);
            }
        }
    }

    public function reqAjaxFormTambahPaket(Request $request)
    {
        if (request()->ajax()) 
        {
            $pketid        = Uuid::uuid4();
            $jenis_peserta = $request->jenis_peserta;
            $nama_paket    = $request->nama_paket;
            $harga_paket   = $request->harga_paket;
            $status        = $request->status;

            $data_paket = [
                                    'pketid'        => $pketid,
                                    'jenis_peserta' => $jenis_peserta,
                                    'nama_paket'    => $nama_paket,
                                    'harga_paket'   => $harga_paket,
                                    'status'        => $status,                                    
                                    'created_at'    => now(),
                                    'updated_at'    => now()
                                ];
            try {
                DB::beginTransaction();
                    DataPaket::insert($data_paket);
                DB::commit();
                return response()->json(['statuslog' => 'success', 'message' => 'Data Paket baru berhasil disimpan', 'title' => 'BERHASIL']);
            } 
            catch (\Exception $e) 
            {
                DB::rollback();
                return response()->json(['statuslog' => 'error', 'message' => 'Gagal menambahkan Data Paket, mohon coba kembali', 'title' => 'GAGAL'], 404);
            }
        }
        else{
            return response()->json(['statuslog'=> 'error' ,'message' => 'Gagal Mengakses Halaman'], 404);
        }
    }

    public function reqAjaxFormUpdatePaket(Request $request)
    {
        if (request()->ajax()) 
        {
            $pketid        = ['pketid' => $request->pketid];
            $jenis_peserta = $request->jenis_peserta;
            $nama_paket    = $request->nama_paket;
            $harga_paket   = $request->harga_paket;
            $status        = $request->status;

            $data_paket = [
                                    'jenis_peserta' => $jenis_peserta,
                                    'nama_paket'    => $nama_paket,
                                    'harga_paket'   => $harga_paket,
                                    'status'        => $status,                                    
                                    'updated_at'    => now()
                                ];
            try {
                DB::beginTransaction();
                    DataPaket::where($pketid)->update($data_paket);
                DB::commit();
                return response()->json(['statuslog' => 'success', 'message' => 'Data Paket berhasil diupdate', 'title' => 'BERHASIL']);
            } 
            catch (\Exception $e) 
            {
                DB::rollback();
                return response()->json(['statuslog' => 'error', 'message' => 'Gagal mengupdate Data Paket, mohon coba kembali', 'title' => 'GAGAL'], 404);
            }
        }
        else{
            return response()->json(['statuslog'=> 'error' ,'message' => 'Gagal Mengakses Halaman'], 404);
        }
    }

    
}
