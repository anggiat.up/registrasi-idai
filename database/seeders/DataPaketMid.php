<?php

namespace Database\Seeders;

use App\Models\DataPaket;
use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;
use Ramsey\Uuid\Uuid;

class DataPaketMid extends Seeder
{
    /**
     * Run the database seeds.
     */
    public function run(): void
    {
        $dataPaket = [
                        

                        [
                            'jenis_peserta' => 'Mahasiswa / Ko-Asisten',
                            'nama_paket'    => 'Symposium',
                            'harga_paket'   => 300000,
                            'status'        => 'Active',
                        ],
                        [
                            'jenis_peserta' => 'Dokter Umum / PPDS',
                            'nama_paket'    => 'Symposium',
                            'harga_paket'   => 400000,
                            'status'        => 'Active',
                        ],
                        [
                            'jenis_peserta' => 'Dokter Spesialis',
                            'nama_paket'    => 'Symposium',
                            'harga_paket'   => 850000,
                            'status'        => 'Active',
                        ],


                        [
                            'jenis_peserta' => 'Mahasiswa / Ko-Asisten',
                            'nama_paket'    => 'Symposium + Workshop 1',
                            'harga_paket'   => 600000,
                            'status'        => 'Active',
                        ],
                        [
                            'jenis_peserta' => 'Dokter Umum / PPDS',
                            'nama_paket'    => 'Symposium + Workshop 1',
                            'harga_paket'   => 1000000,
                            'status'        => 'Active',
                        ],
                        [
                            'jenis_peserta' => 'Dokter Spesialis',
                            'nama_paket'    => 'Symposium + Workshop 1',
                            'harga_paket'   => 1750000,
                            'status'        => 'Active',
                        ],
                        [
                            'jenis_peserta' => 'Mahasiswa / Ko-Asisten',
                            'nama_paket'    => 'Symposium + Workshop 2',
                            'harga_paket'   => 600000,
                            'status'        => 'Active',
                        ],
                        [
                            'jenis_peserta' => 'Dokter Umum / PPDS',
                            'nama_paket'    => 'Symposium + Workshop 2',
                            'harga_paket'   => 1000000,
                            'status'        => 'Active',
                        ],
                        [
                            'jenis_peserta' => 'Dokter Spesialis',
                            'nama_paket'    => 'Symposium + Workshop 2',
                            'harga_paket'   => 1750000,
                            'status'        => 'Active',
                        ],
                        [
                            'jenis_peserta' => 'Mahasiswa / Ko-Asisten',
                            'nama_paket'    => 'Symposium + Workshop 3',
                            'harga_paket'   => 600000,
                            'status'        => 'Active',
                        ],
                        [
                            'jenis_peserta' => 'Dokter Umum / PPDS',
                            'nama_paket'    => 'Symposium + Workshop 3',
                            'harga_paket'   => 1000000,
                            'status'        => 'Active',
                        ],
                        [
                            'jenis_peserta' => 'Dokter Spesialis',
                            'nama_paket'    => 'Symposium + Workshop 3',
                            'harga_paket'   => 1750000,
                            'status'        => 'Active',
                        ],
                        [
                            'jenis_peserta' => 'Mahasiswa / Ko-Asisten',
                            'nama_paket'    => 'Symposium + Workshop 4',
                            'harga_paket'   => 600000,
                            'status'        => 'Active',
                        ],
                        [
                            'jenis_peserta' => 'Dokter Umum / PPDS',
                            'nama_paket'    => 'Symposium + Workshop 4',
                            'harga_paket'   => 1000000,
                            'status'        => 'Active',
                        ],
                        [
                            'jenis_peserta' => 'Dokter Spesialis',
                            'nama_paket'    => 'Symposium + Workshop 4',
                            'harga_paket'   => 1750000,
                            'status'        => 'Active',
                        ]
                       
            ];

        foreach ($dataPaket as $paket) {
            DataPaket::create([
                'pketid'        => Uuid::uuid4(),
                'jenis_peserta' => $paket['jenis_peserta'],
                'nama_paket'    => $paket['nama_paket'],
                'harga_paket'   => $paket['harga_paket'],
                'waktu'         => time(),
                'status'        => $paket['status'],
                'created_at'    => now(),
                'updated_at'    => now(),
            ]);
        }        
    }
}
