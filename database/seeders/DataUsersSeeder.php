<?php

namespace Database\Seeders;

use App\Models\DataUsers;
use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;
use Ramsey\Uuid\Uuid;

class DataUsersSeeder extends Seeder
{
    /**
     * Run the database seeds.
     */
    public function run(): void
    {
        // Tambahkan data ke dalam tabel data_users
        DataUsers::create(
            [
                'userid'        => Uuid::uuid4(),
                'email'        => 'admin@gmail.com',
                'password'     => bcrypt('admin2024'),
                'nama_lengkap' => 'Administrator',
                'no_hp'        => '08123456789',
                'level_akun'   => 'admin',
                'status_akun'  => 'aktif',
            ],
            [
                'userid'        => Uuid::uuid4(),
                'email'        => 'nadia@gmail.com',
                'password'     => bcrypt('nadia123'),
                'nama_lengkap' => 'Nadia Ucnh',
                'no_hp'        => '08123456789',
                'level_akun'   => 'user',
                'status_akun'  => 'aktif',
            ]
        );
        
    }
}
