<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('data_paketharga', function (Blueprint $table) {
            $table->uuid('pketid')->primary();
            $table->string('jenis_peserta');
            $table->string('nama_paket');
            $table->integer('harga_paket')->nullable();
            $table->integer('waktu')->nullable();
            $table->enum('status', ['Active', 'Inactive'])->default('Active');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('data_paketharga');
    }
};
