<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('data_users', function (Blueprint $table) {
            $table->uuid('userid', 36)->primary();
            $table->string('email')->unique();
            $table->string('password', 100);
            $table->string('nama_lengkap', 150);
            $table->char('no_hp', 20);
            $table->enum('level_akun', ['admin', 'operator1', 'operator2'])->default('operator1');
            $table->enum('status_akun', ['aktif', 'tidak-aktif'])->default('aktif');
            $table->integer('terakhir_login')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('data_users');
    }
};
