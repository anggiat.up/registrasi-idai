<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('data_pendaftaran', function (Blueprint $table) {
            $table->uuid('pdftrid')->primary();
            $table->char('kode_invoice', 14)->unique();
            $table->json('data_pendaftaran')->nullable();
            $table->integer('total_biaya')->nullable();
            $table->integer('kode_unik')->nullable();
            $table->enum('status_pembayaran', ['menunggu', 'terverifikasi', 'ditolak'])->default('menunggu');
            $table->string('bukti_pembayaran')->nullable();
            $table->char('admin_verifikasi', 36)->nullable();
            $table->timestamps();
            
            $table->foreign('admin_verifikasi')->references('userid')->on('data_users')->onUpdate('cascade')->onDelete('restrict');
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('data_pendaftaran');
    }
};
