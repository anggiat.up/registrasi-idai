<!DOCTYPE html>
<html lang="en">

<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <meta name="description" content="An impressive and flawless site template that includes various UI elements and countless features, attractive ready-made blocks and rich pages, basically everything you need to create a unique and professional website.">
  <meta name="keywords" content="bootstrap 5, business, corporate, creative, gulp, marketing, minimal, modern, multipurpose, one page, responsive, saas, sass, seo, startup, html5 template, site template">
  <meta name="author" content="elemis">
  <title>3rd RiPiU - Riau Pediatric Update</title>
  <link rel="shortcut icon" href="./img/Idai-riau.ico">
  <link rel="stylesheet" href="./css/plugins.css">
  <link rel="stylesheet" href="./css/style.css">
  <link rel="stylesheet" href="./css/colors/yellow.css">
  <link rel="preload" href="./css/fonts/thicccboi.css" as="style" onload="this.rel='stylesheet'">
  <link href="{{ asset('sistems/libs/sweetalert2/sweetalert2.min.css') }}" rel="stylesheet" type="text/css" />
</head>
<style>
    /* body {
        background-image: url(../img/floral-seamless-pattern.jpg) !important;
        background-repeat: repeat;
        background-size: 25% auto;
        background-color: rgba(255, 255, 255, 0.4);
    } */
</style>
<body>
  <div class="content-wrapper">
    <header class="wrapper bg-soft-primary">
  	<div id="base-url" data-baseurl="{{ url('') }}" ></div>
    <div id="loading-ajax">Loading...</div>
	<style>
		#loading-ajax{
			width: 100%;
			height: 100%;
			position: fixed;
			text-indent: 100%;
			margin-top: 0px;
			background: #e0e0e0 url("{{ asset('img/loading2.gif') }}") no-repeat center;
			z-index: 1321050 ;
			opacity: 0.6;
			background-size: 20%;
		}
	</style>

      <nav class="navbar navbar-expand-lg center-nav transparent navbar-light">
        <div class="container flex-lg-row flex-nowrap align-items-center">
          <div class="navbar-brand w-100">
            <a href="{{ url('/') }}">
              <img src="./img/logo-dark.png" srcset="./img/logo-dark@2x.png 2x" alt="" />
            </a>
          </div>
          <div class="navbar-collapse offcanvas offcanvas-nav offcanvas-start">
            <div class="offcanvas-header d-lg-none">
              <h3 class="text-white fs-30 mb-0">Sandbox</h3>
              <button type="button" class="btn-close btn-close-white" data-bs-dismiss="offcanvas" aria-label="Close"></button>
            </div>
            <div class="offcanvas-body ms-lg-auto d-flex flex-column h-100">
              <ul class="navbar-nav">
                <li class="nav-item ">
                    <a class="nav-link " href="{{ url('/') }}" >Home</a>
                </li>
                <li class="nav-item ">
                    <a class="nav-link " href="{{ url('/home#materi-ripiu') }}" >Materi RiPiU 2024</a>
                </li>
                <li class="nav-item ">
                    <a class="nav-link " href="{{ url('/home#biaya-pendaftaran') }}" >Biaya Pendaftaran</a>
                </li>
                <li class="nav-item ">
                    <a class="nav-link " href="{{ url('registrasi') }}" >Registrasi</a>
                </li>
                <li class="nav-item ">
                  <a class="nav-link " href="{{ url('sertifikat') }}" >Sertifikat</a>
              </li>
                <li class="nav-item ">
                    <a class="nav-link " href="{{ url('kontak-kami') }}" >Kontak Kami</a>
                </li>                                            
              </ul>
              <!-- /.navbar-nav -->
              <div class="offcanvas-footer d-lg-none">
                <div>
                  <a href="mailto:first.last@email.com" class="link-inverse">info@email.com</a>
                  <br /> 00 (123) 456 78 90 <br />
                  <nav class="nav social social-white mt-4">
                    <a href="#"><i class="uil uil-twitter"></i></a>
                    <a href="#"><i class="uil uil-facebook-f"></i></a>
                    <a href="#"><i class="uil uil-dribbble"></i></a>
                    <a href="#"><i class="uil uil-instagram"></i></a>
                    <a href="#"><i class="uil uil-youtube"></i></a>
                  </nav>
                  <!-- /.social -->
                </div>
              </div>
              <!-- /.offcanvas-footer -->
            </div>
            <!-- /.offcanvas-body -->
          </div>
          <!-- /.navbar-collapse -->
          <div class="navbar-other w-100 d-flex ms-auto">
            <ul class="navbar-nav flex-row align-items-center ms-auto">              
              <li class="nav-item d-lg-none">
                <button class="hamburger offcanvas-nav-btn"><span></span></button>
              </li>
            </ul>
            <!-- /.navbar-nav -->
          </div>
          <!-- /.navbar-other -->
        </div>
        <!-- /.container -->
      </nav>
      <!-- /.navbar -->
      
      <!-- /.offcanvas -->
    </header>
    <!-- /header -->

    @yield('container')
    
  </div>
  <!-- /.content-wrapper -->
  <footer class="bg-navy text-inverse">
    <div class="container pt-15 pt-md-17 pb-13 pb-md-15">
      <div class="d-lg-flex flex-row align-items-lg-center">
        <h3 class="display-4 mb-6 mb-lg-0 pe-lg-20 pe-xl-22 pe-xxl-25 text-white">Pediatric Emergies in Primary and Tertiary Care : What Should Clinicians Know ? </h3>
        <a href="{{ url('registrasi') }}" class="btn btn-primary rounded-pill mb-0 text-nowrap">Registrasi Sekarang</a>
      </div>
      <!--/div -->
      <hr class="mt-11 mb-12" />
      <div class="row gy-6 gy-lg-0">
        <div class="col-md-12 col-lg-12 text-center">
          <div class="widget">
            <img class="mb-4" src="./img/logo-light.png" srcset="./img/logo-idai-dark.png" width="250px" alt="" />
            <p class="mb-4">© 2024 RiPiU 3 By <a href="javascript:void(0);" data-bs-toggle="modal" data-bs-target="#modal-narapati">Narapati Project</a>.</p>
            
          </div>
          <!-- /.widget -->
        </div>
        <!-- /column -->
        
        <!-- /column -->
        
        <!-- /column -->
      </div>
      <!--/.row -->
    </div>
    <!-- /.container -->
    <div class="modal fade modal-large" id="modal-narapati" tabindex="-1" role="dialog" aria-labelledby="modal-narapati" aria-hidden="true">
			<div class="modal-dialog modal-lg" role="document" >
				<div class="modal-content" style="background-color:#fff0;box-shadow: 0 0 40px 0 rgb(0 0 0 / 0%);">
					<div class="modal-body z-bigger">
						<div class="container-fluid">
							
							<div class="row justify-content-center">
								
								
									{{-- <img class="border-4" src="img/img-1.jpg" alt=""> 													 --}}
									<img class="border-4" src="{{ asset('img/narapati.png') }}" alt="" style="width: 100%;">
								{{-- <iframe id="pdfViewer" src="{{ url('documents/Call for Abstract.pdf') }}" width="100%" height="600px"></iframe> --}}
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
  </footer>
  <div class="progress-wrap">
    <svg class="progress-circle svg-content" width="100%" height="100%" viewBox="-1 -1 102 102">
      <path d="M50,1 a49,49 0 0,1 0,98 a49,49 0 0,1 0,-98" />
    </svg>
  </div>
  <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.7.1/jquery.min.js" integrity="sha512-v2CJ7UaYy4JwqLDIrZUI/4hqeoQieOmAZNXBeQyjo21dadnwR+8ZaIJVT8EE2iyI61OV8e6M8PP2/4hpQINQ/g==" crossorigin="anonymous" referrerpolicy="no-referrer"></script>
  <!-- Sweet Alerts js -->
	<script src="{{ asset('sistems/libs/sweetalert2/sweetalert2.min.js') }}"></script>

	<!-- Sweet alert init js-->
	<script src="{{ asset('sistems/js/pages/sweet-alerts.init.js') }}"></script> 
  <script src="./js/plugins.js"></script>
  <script src="./js/theme.js"></script>
  <script src="{{ asset('js/public-custom.js?v='.filemtime(public_path('js/public-custom.js'))) }}"></script>  

  <script>
    var countDownDate = new Date("Jul 28, 2024 00:00:01").getTime();

    // Update the count down every 1 second
    var x = setInterval(function() {

      // Get today's date and time
      var now = new Date().getTime();
        
      // Find the distance between now and the count down date
      var distance = countDownDate - now;
        
      // Time calculations for days, hours, minutes and seconds
      var days = Math.floor(distance / (1000 * 60 * 60 * 24));
      var hours = Math.floor((distance % (1000 * 60 * 60 * 24)) / (1000 * 60 * 60));
      var minutes = Math.floor((distance % (1000 * 60 * 60)) / (1000 * 60));
      var seconds = Math.floor((distance % (1000 * 60)) / 1000);
        
      // Output the result in an element with id="demo"
      
      if (document.getElementById("hari")) {
        
        document.getElementById("hari").innerHTML = 0;
        document.getElementById("jam").innerHTML = 0;
        document.getElementById("menit").innerHTML = 0;
        document.getElementById("detik").innerHTML = 0;
      }
        
      // If the count down is over, write some text 
      
    }, 1000);
  </script>
  
</body>

</html>