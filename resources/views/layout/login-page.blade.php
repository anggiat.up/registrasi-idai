<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8" />
        <title>Log In | RiPiU3 2024</title>
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta content="Sistem RiPiU3 2024" name="description" />
        <meta content="Registrasi" name="Anggiat" />
        <meta http-equiv="X-UA-Compatible" content="IE=edge" />
        <!-- App favicon -->
        <link rel="shortcut icon" href="{{ asset('sistems/images/Idai-riau.ico') }}">

        <!-- Notification css (Toastr) -->
        <link href="{{ asset('sistems/libs/toastr/toastr.min.css') }}" rel="stylesheet" type="text/css" />

        <!-- App css -->
        <link href="{{ asset('sistems/css/bootstrap.min.css') }}" rel="stylesheet" type="text/css" id="bootstrap-stylesheet" />
        <link href="{{ asset('sistems/css/icons.min.css') }}" rel="stylesheet" type="text/css" />
        <link href="{{ asset('sistems/css/app.min.css') }}" rel="stylesheet" type="text/css"  id="app-stylesheet" />

    </head>

    <body class="authentication-bg">
        <div id="base-url" data-baseurl="{{ url('') }}" ></div>
        <div class="account-pages pt-5 my-5">
            <div class="container">
                <div class="row justify-content-center">
                    <div class="col-md-8 col-lg-6 col-xl-5">
                        <div class="account-card-box">
                            <div class="card mb-0">
                                <div class="card-body p-4">
                                    
                                    <div class="text-center">
                                        <div class="my-3">
                                            <a href="{{ url('') }}">
                                                <span><img src="{{ asset('sistems/images/logo-dark.png') }}" alt="" height="68"></span>
                                            </a>
                                        </div>
                                        <h5 class="text-muted text-uppercase py-3 font-16">Sign In</h5>
                                    </div>
    
                                    <form action="#" class="mt-2" id="form-login">
                                        @csrf
                                        <div class="form-group mb-3">
                                            <input class="form-control" type="email" name="email" required="" placeholder="Enter your email">
                                        </div>
    
                                        <div class="form-group mb-3">
                                            <input class="form-control" type="password" name="password" required="" id="password" placeholder="Enter your password">
                                        </div>
    
                                        
    
                                        <div class="form-group text-center">
                                            <button class="btn btn-success btn-block waves-effect waves-light" type="submit"> Log In </button>
                                        </div>

                                        
    
                                    </form>

                                    
    
                                </div> <!-- end card-body -->
                            </div>
                            <!-- end card -->
                        </div>

                        
                        <!-- end row -->

                    </div> <!-- end col -->
                </div>
                <!-- end row -->
            </div>
            <!-- end container -->
        </div>
        <!-- end page -->

        <!-- Vendor js -->
        <script src="{{ asset('sistems/js/vendor.min.js') }}"></script>

        <!-- Toastr js -->
        <script src="{{ asset('sistems/libs/toastr/toastr.min.js') }}"></script>

        <script src="{{ asset('sistems/js/pages/toastr.init.js') }}"></script>

        <!-- App js -->
        <script src="{{ asset('sistems/js/app.min.js') }}"></script>
        <script src="{{ asset('sistems/js/customs.js') }}"></script>
        
    </body>
</html>