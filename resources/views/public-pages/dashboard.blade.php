@extends('layout.mainbody')

@section('container')


  <section class="wrapper bg-soft-primary angled lower-start">
    <div class="container pt-5 pb-15 py-lg-17 py-xl-19 pb-xl-20 position-relative">
      <img class="position-lg-absolute col-12 col-lg-10 col-xl-11 col-xxl-10 px-lg-5 px-xl-0 ms-n5 ms-sm-n8 ms-md-n10 ms-lg-0 mb-md-4 mb-lg-0" src="./img/photos/RiPiU3-Image.png" srcset="./img/RiPiU3-Image.png" data-cue="fadeIn" alt="" style="top: -1%; left: -21%;" />
      <div class="row gx-0 align-items-center">
        <div class="col-md-10 offset-md-1 col-lg-5 offset-lg-7 offset-xxl-6 ps-xxl-12 mt-md-n9 text-center text-lg-start" data-cues="slideInDown" data-group="page-title" data-delay="600">
          <h1 class="display-5 mb-4 mx-sm-n2 mx-md-0">3<sup>rd</sup> RIAU PEDIATRIC UPDATE</h1>
          <p class="lead fs-lg mb-7 px-md-10 px-lg-0">Pediatric Emergies in Primary and Tertiary Care : What Should Clinicians Know ?</p>
          <div class="row align-items-center counter-wrapper gy-4 gy-md-0">

            <div class="col-md-3 text-center">
              <h3 id="hari" class="counter counter-lg text-primary">0</h3>
              <p>Hari</p>
            </div>
             
            <div class="col-md-3 text-center">
              <h3 id="jam" class="counter counter-lg text-primary">0</h3>
              <p>Jam</p>
            </div>
             
            <div class="col-md-3 text-center">
              <h3 id="menit" class="counter counter-lg text-primary">0</h3>
              <p>Menit</p>
            </div>

            <div class="col-md-3 text-center">
              <h3 id="detik" class="counter counter-lg text-primary">0</h3>
              <p>Detik</p>
            </div>
            <!--/column -->
          </div>
          <div class=" text-center mt-8" data-cues="slideInDown" data-group="page-title-buttons" data-delay="900">
            <span><a href="{{ url('registrasi') }}" class="btn btn-green btn-icon btn-icon-start rounded"><i class="uil uil-file-edit-alt"></i> Register Now</a></span>
          </div>
        </div>
        <!-- /column -->
      </div>
      <!-- /.row -->
    </div>
    <!-- /.container -->
  </section>
    
    <!-- /section -->

    <section class="wrapper image-wrapper bg-auto no-overlay bg-image text-center bg-map " data-image-src="./img/map.png">
      <div class="container py-14 pt-md-16 pb-md-18">
        <div class="row pt-md-12">
          <div class="col-md-9 col-lg-7 col-xl-7 mx-auto text-center">
            <img src="./img/icons/lineal/puzzle-2.svg" class="svg-inject icon-svg icon-svg-md mb-4" alt="" />
            <h2 class="display-2 mb-3">DOWNLOAD CERTIFICATE</h2>
            <p class="lead fs-lg mb-6 px-xl-10 px-xxl-15">Sertifikat telah tersedia untuk dicetak, silahkan generate dan download sekarang</p>
            <span><a href="{{ url('sertifikat') }}" class="btn btn-primary btn-icon btn-icon-start rounded"><i class="uil uil-file-edit-alt"></i> Cetak Sertifikat</a></span>
        </div>
        </div>
        
      </div>
    </section>


    {{-- Materi RiPiU3 --}}
    <section class="wrapper  bg-pale-primary angled lower-start upper-end" id="materi-ripiu">
        <div class="container py-14 py-md-16">
                    
          <div class="grid">
            <div class="row isotope gy-6">
              <div class="item col-md-6 col-xl-6 text-center">
                <h3 class="display-4 mb-10 px-xl-10 px-xxl-15">Symposium</h3>
                <div class="card">
                  <div class="card-body">                    
                        
                        <div class="info text-center">
                            <h5 class="mb-1">ERIA</h5>
                            "Update on Septic Shock Management in Children"
                            <hr class="m-0"> <br>
                        </div>
                        <div class="info text-center">
                            <h5 class="mb-1">NEONATOLOGI</h5>
                            "Update Management on Perinatal Asphyxia"
                            <hr class="m-0"> <br>
                        </div>
                        <div class="info text-center">
                            <h5 class="mb-1">NEUROLOGI</h5>
                            "Update on Status Epilepticus in Children"
                            <hr class="m-0"> <br>
                        </div>
                        <div class="info text-center">
                            <h5 class="mb-1">RESPIROLOGI</h5>
                            "Update on Asthma Attack in Children"
                            <hr class="m-0"> <br>
                        </div>
                        <div class="info text-center">
                            <h5 class="mb-1">HEMATO-ONKOLOGI</h5>
                            "Update on Haemorrhage in Children"
                            <hr class="m-0"> <br>
                        </div>
                        <div class="info text-center">
                            <h5 class="mb-1">NEFROLOGI</h5>
                            "Update on Acute Kidney Injury (AKI) in Children"
                            <hr class="m-0"> <br>
                        </div>
                        <div class="info text-center">
                            <h5 class="mb-1">GASTROENTEROHEPATOLOGI</h5>
                            "Update on Abdominal Pain in Children"
                            <hr class="m-0"> <br>
                        </div>
                        <div class="info text-center">
                            <h5 class="mb-1">NUTRISI & PENYAKIT METABOLIK</h5>
                            "Early Diagnosis and Management of Stunting"
                            <hr class="m-0"> <br>
                        </div>

                        
                  </div>
                </div>
              </div>
              <div class="item col-md-6 col-xl-6 text-center">
                <h3 class="display-4 mb-10 px-xl-10 px-xxl-15">Workshop</h3>
                <div class="card">
                  <div class="card-body">                    
                        
                        <div class="info text-center">
                            <h5 class="mb-1">1. ERIA + NEUROPEDIATRI</h5>
                            "Management of Neuro-Intensive Problem in Daily Practice : Focus on Status Epilepticus"
                            <hr class="m-0"> <br>
                        </div>
                        <div class="info text-center">
                            <h5 class="mb-1">2. NEONATOLOGI</h5>
                            "Therapeutic Hypothermia in Perinatal Asphyxia"
                            <hr class="m-0"> <br>
                        </div>
                        <div class="info text-center">
                            <h5 class="mb-1">3. GASTROHEPATOLOGI + BEDAH ANAK</h5>
                            "Sakit Perut pada Anak: Kapan Harus Memikirkan Kelainan Bedah?"
                            <hr class="m-0"> <br>
                        </div>
                        <div class="info text-center">
                            <h5 class="mb-1">4. HEMATO-ONKOLOGI</h5>
                            "Pediatric Bleeding Management: The Art of Clinical Decision Making"
                            <hr class="m-0"> <br>
                        </div>

                        
                  </div>
                </div>
              </div>
            </div>
            <!-- /.row -->
          </div>
          
        </div>
    </section>
    
    {{-- Biaya Pendaftaran --}}
    <section class="wrapper bg-light" id="biaya-pendaftaran">
        <div class="container py-14 py-md-16">
          <h2 class="display-4 mb-3 text-center">Biaya Pendaftaran Early-Bird</h2>
          <p class="lead fs-lg text-center">Biaya pendaftaran Early-Bird berakhir sampai tanggal 15 Juni 2024</p>          
          <!--/.row -->
          <div class="table-responsive">
            <table class="table table-borderless table-striped text-center text-dark">
              <thead>
                <tr>
                  <th class="w-25"></th>
                  <th>
                    <div class="h4 mb-1">Symposium</div>
                  </th>
                  <th>
                    <div class="h4 mb-1">Symposium + Workshop</div>
                  </th>
                </tr>
              </thead>
              <tbody>
                <tr>
                  <td class="option text-start">Mahasiswa / Ko-Asisten</td>
                  <td>Rp 250.000,-</td>
                  <td>Rp 500.000,-</td>
                </tr>
                <tr>
                  <td class="option text-start">Dokter Umum / PPDS</td>
                  <td>Rp 300.000,-</td>
                  <td>Rp 750.000,-</td>
                </tr>
                <tr>
                  <td class="option text-start">Dokter Spesialis</td>
                  <td>Rp 750.000,-</td>
                  <td>Rp 1.400.000,-</td>
                </tr>
              </tbody>
            </table>
          </div>

          <h2 class="display-4 mb-3 text-center mt-10">Biaya Pendaftaran Regular</h2>
          <!--/.row -->
          <div class="table-responsive">
            <table class="table table-borderless table-striped text-center">
              <thead>
                <tr>
                  <th class="w-25"></th>
                  <th>
                    <div class="h4 mb-1">Symposium</div>
                  </th>
                  <th>
                    <div class="h4 mb-1">Symposium + Workshop</div>
                  </th>
                </tr>
              </thead>
              <tbody>
                <tr>
                  <td class="option text-start">Mahasiswa / Ko-Asisten</td>
                  <td>Rp 300.000,-</td>
                  <td>Rp 600.000,-</td>
                </tr>
                <tr>
                  <td class="option text-start">Dokter Umum / PPDS</td>
                  <td>Rp 400.000,-</td>
                  <td>Rp 1.000.000,-</td>
                </tr>
                <tr>
                  <td class="option text-start">Dokter Spesialis</td>
                  <td>Rp 850.000,-</td>
                  <td>Rp 1.750.000,-</td>
                </tr>
              </tbody>
            </table>
          </div>
        </div>
    </section>

    
    {{-- Harga Paket --}}
    {{-- <section class="wrapper bg-light">
        <div class="container py-14 py-md-16 text-center">
            <div class="container pb-14 pt-14 pb-md-16">
                <div class="pricing-wrapper position-relative mt-n18 mt-md-n21 mb-12 mb-md-15">
                <div class="shape bg-dot primary rellax w-16 h-18" data-rellax-speed="1" style="top: 2rem; right: -2.4rem;"></div>
                <div class="shape rounded-circle bg-line red rellax w-18 h-18 d-none d-lg-block" data-rellax-speed="1" style="bottom: 0.5rem; left: -2.5rem;"></div>
                
                <div class="row gy-6 mt-3 mt-md-5">
                    <div class="col-md-6 col-lg-4 offset-lg-2">
                        <div class="pricing card text-center">
                            <div class="card-body">
                            <img src="./img/icons/lineal/shopping-basket.svg" class="svg-inject icon-svg icon-svg-md text-primary mb-3" alt="" />
                            <h4 class="card-title">Basic Plan</h4>
                            <div class="prices text-dark">
                                <div class="price price-show"><span class="price-currency">$</span><span class="price-value">9</span> <span class="price-duration">mo</span></div>
                                <div class="price price-hide price-hidden"><span class="price-currency">$</span><span class="price-value">99</span> <span class="price-duration">yr</span></div>
                            </div>
                            <!--/.prices -->
                            <ul class="icon-list bullet-bg bullet-soft-primary mt-7 mb-8 text-start">
                                <li><i class="uil uil-check"></i><span><strong>1</strong> Project </span></li>
                                <li><i class="uil uil-check"></i><span><strong>100K</strong> API Access </span></li>
                                <li><i class="uil uil-check"></i><span><strong>100MB</strong> Storage </span></li>
                                <li><i class="uil uil-times bullet-soft-red"></i><span> Weekly <strong>Reports</strong> </span></li>
                                <li><i class="uil uil-times bullet-soft-red"></i><span> 7/24 <strong>Support</strong></span></li>
                            </ul>
                            <a href="#" class="btn btn-primary rounded-pill">Choose Plan</a>
                            </div>
                            <!--/.card-body -->
                        </div>
                    </div>
                    
                    <div class="col-md-6 col-lg-4 offset-lg-0">
                        <div class="pricing card text-center">
                            <div class="card-body">
                            <img src="./img/icons/lineal/briefcase-2.svg" class="svg-inject icon-svg icon-svg-md text-primary mb-3" alt="" />
                            <h4 class="card-title">Corporate Plan</h4>
                            <div class="prices text-dark">
                                <div class="price price-show"><span class="price-currency">$</span><span class="price-value">49</span> <span class="price-duration">mo</span></div>
                                <div class="price price-hide price-hidden"><span class="price-currency">$</span><span class="price-value">499</span> <span class="price-duration">yr</span></div>
                            </div>
                            <!--/.prices -->
                            <ul class="icon-list bullet-bg bullet-soft-primary mt-7 mb-8 text-start">
                                <li><i class="uil uil-check"></i><span><strong>20</strong> Projects </span></li>
                                <li><i class="uil uil-check"></i><span><strong>300K</strong> API Access </span></li>
                                <li><i class="uil uil-check"></i><span><strong>500MB</strong> Storage </span></li>
                                <li><i class="uil uil-check"></i><span> Weekly <strong>Reports</strong></span></li>
                                <li><i class="uil uil-check"></i><span> 7/24 <strong>Support</strong></span></li>
                            </ul>
                            <a href="#" class="btn btn-primary rounded-pill">Choose Plan</a>
                            </div>
                            <!--/.card-body -->
                        </div>
                        <!--/.pricing -->
                    </div>
                    <!--/column -->
                </div>
                <!--/.row -->
                </div>
                <!--/.pricing-wrapper -->
                
                
            </div>
        </div>
        <!-- /.container -->
    </section> --}}

    


  @endsection