@extends('layout.mainbody')

@section('container')
<section class="wrapper bg-light">
    <div class="container py-14 py-md-16">
      <div class="row gx-md-8 gx-xl-12 gy-10 align-items-center">
        <div class="col-md-8 col-lg-6 offset-lg-0 col-xl-5 offset-xl-1 position-relative">
          <div class="shape bg-dot primary rellax w-17 h-21" data-rellax-speed="1" style="top: -2rem; left: -1.4rem;"></div>
          <figure class="rounded"><img src="./img/illustrations/i14.png" srcset="./img/photos/about4@2x.jpg 2x" alt=""></figure>
        </div>
        <!--/column -->
        <div class="col-lg-6">
          <img src="./img/icons/lineal/telemarketer.svg" class="svg-inject icon-svg icon-svg-md mb-4" alt="" />
          <h2 class="display-4 mb-8">Need more information? Please contact us.</h2>
          
          <div class="d-flex flex-row">
            <div>
              <div class="icon text-primary fs-28 me-6 mt-n1"> <i class="uil uil-phone-volume"></i> </div>
            </div>
            <div>
              <h5 class="mb-1">Phone</h5>
              <p>Nadia :  0822-8360-8207</p>
            </div>
          </div>
          <div class="d-flex flex-row">
            <div>
              <div class="icon text-primary fs-28 me-6 mt-n1"> <i class="uil uil-envelope"></i> </div>
            </div>
            <div>
              <h5 class="mb-1">Email</h5>
              <p><a href="mailto:idai.riau@idai.or.id" class="link-body">idai.riau@idai.or.id</a></p>
            </div>
          </div>
          <div class="d-flex flex-row">
            <div>
              <div class="icon text-primary fs-28 me-6 mt-n1"> <i class="uil uil-instagram"></i> </div>
            </div>
            <div>
              <h5 class="mb-1">Instagram</h5>
              <p><a href="https://www.instagram.com/idairiau/">@idairiau</a></p>
            </div>
          </div>
          
        </div>
        <!--/column -->
      </div>
      <!--/.row -->
    </div>
    <!-- /.container -->
  </section>
  <!-- /section -->
@endsection