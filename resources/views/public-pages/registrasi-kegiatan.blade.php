@extends('layout.mainbody')

@section('container')

    <section class="wrapper bg-soft-primary">
        <div class="container pt-10 pb-4 pt-md-14 pb-md-10 text-center">
        <div class="row">
            <div class="col-sm-10 col-md-8 col-lg-6 col-xl-6 col-xxl-5 mx-auto">
                <h1 class="display-1 mb-3">Registrasi RiPiU 2024</h1>            
            </div>
            <!-- /column -->
        </div>
        <!-- /.row -->
        </div>
    </section>

        <section class="wrapper bg-primary">
            <div class="container py-14 py-md-16">
          {{-- <div class="row mb-3">
            <div class="col-md-10 col-lg-12 col-xl-10 col-xxl-9 mx-auto text-center">              
              <h3 class="display-4  px-lg-19 px-xl-18">Registrasi & Upload Bukti Pembayaran</h3>
            </div>
          </div> --}}
          <!--/.row -->
          <div class="row grid-view gx-md-8 gx-xl-10 gy-8 gy-lg-0">
            <section id="snippet-1" class="wrapper ">
                <div class="card">
                  <div class="card-body">

                    
                    <ul class="nav nav-tabs nav-pills">
                        <li class="nav-item"> 
                          <a class="nav-link active" data-bs-toggle="tab" href="#tab1-1">
                            <i class="uil uil-comment-question pe-1"></i>
                            <span>Tata Cara Pembayaran</span>
                          </a>
                        </li>
                        <li class="nav-item"> 
                          <a class="nav-link" data-bs-toggle="tab" href="#tab1-2">
                            <i class="uil uil-file-plus-alt pe-1"></i>
                            <span>Registrasi</span>
                          </a> 
                        </li>
                        <li class="nav-item"> 
                          <a class="nav-link" data-bs-toggle="tab" href="#tab1-3">
                            <i class="uil uil-bill pe-1"></i>
                            <span>Upload Bukti Pembayaran</span>
                          </a> 
                        </li>
                    </ul>
                      
                      
                    <div class="tab-content">
                        <div class="tab-pane fade show active" id="tab1-1">
                            <div class="col-12 text-dark">
                                <h2 class="mb-2 text-center">Petunjuk & Tata Cara Pembayaran</h2>
                                <p>
                                    <ol>
                                        <li><h6>Lengkapi Formulir Pendaftaran</h6>
                                            <ul>
                                                <li>Isilah formulir pendaftaran dengan data peserta yang valid.</li>
                                                <li>Harap menggunakan email aktif untuk menerima notifikasi pembayaran dan informasi terkait acara.</li>
                                            </ul>
                                        </li>
                                        <li><h6>Tata Cara Pembayaran</h6>
                                            <ul>
                                                <li>Setelah mengisi formulir dan meng-submit pendaftaran, Anda akan menerima notifikasi melalui email yang berisi nomor Invoice dan jumlah pembayaran yang harus dibayarkan sesuai dengan jenis kegiatan dan jenis peserta yang anda daftarkan.</li>
                                                <li>Saat proses pembayaran pastikan nominal harus sesuai dengan jumlah yang harus dibayarkan dan kode unik yang tertera melalui email.</li>
                                                <li>Pada bagian berita acara, pastikan Anda mengisi berita acara dengan <b>NOMOR INVOICE</b> yang diterima melalui email.</li>
                                                <li>Pembayaran yang telah dilakukan <b class="text-danger">TIDAK DAPAT DIBATALKAN</b> atau <b class="text-danger">REFUND</b>.</li>
                                            </ul>
                                        </li>
                                        <li><h6>Konfirmasi Pembayaran</h6>
                                            <ul>
                                                <li>Pembayaran dilakukan melalui : 
                                                    <table width="100%" class="ml-3">
                                                        <tr>
                                                            <th width="20%" >Nama BANK</th>
                                                            <th width="5%">:</th>
                                                            <td>BRI (Bank Rakyat Indonesia)</td>
                                                        </tr>
                                                        <tr>
                                                            <th>Nama Rekening</th>
                                                            <th >:</th>
                                                            <td>PANITIA RIPIU 3</td>
                                                        </tr>
                                                        <tr>
                                                            <th>Nomor Rekening</th>
                                                            <th >:</th>
                                                            <td>1720-01-002823-53-0</td>
                                                        </tr>
                                                    </table>
                                                    
                                                </li>
                                                <li>Setelah melakukan pembayaran, Anda dapat mengunggah bukti transfer melalui formulir konfirmasi pembayaran.</li>
                                                <li>Setelah konfirmasi pembayaran diterima, Anda akan menerima notifikasi pembayaran Anda.</li>
                                                <li>Proses konfirmasi pembayaran akan memakan waktu hingga 2x24 jam.</li>
                                            </ul>
                                        </li>
                                        <li><h6>Kontak Admin</h6>
                                            <ul>
                                                <table width="100%" class="ml-3">
                                                    <tr>
                                                        <th width="20%" >Nadia</th>
                                                        <th width="5%">:</th>
                                                        <td><a class="text-dark" href="wa.me/6282283608207">+62 822 8360 8207</a></td>
                                                    </tr>
                                                </table>
                                                
                                            </ul>
                                        </li>
                                    </ol>         
                                </p>

                            </div>
                        </div>
                        
                        <div class="tab-pane fade" id="tab1-2">
                            <form id="form-registrasi" action="#" method="post">
                                @csrf
                                <div class="row">
                                    <h2 class="mb-2 text-center">Form Registrasi RiPiU 2024</h2>

                                        <div class="col-md-12 mt-3 mb-0 "> 
                                            <div class="form-group">
                                                <label for="jenis-partisipasi" class="text-dark font-weight-bold">Jenis Peserta<span class="text-danger">*</span></label>
                                                <select class="form-select text-dark" name="jenis-partisipasi" id="jenis-partisipasi" aria-label="Default select example">
                                                    <option selected>-- Pilih jenis peserta --</option>
                                                    <option value="Mahasiswa / Ko-Asisten">Mahasiswa / Ko-Asisten</option>
                                                    <option value="Dokter Umum / PPDS">Dokter Umum / PPDS</option>
                                                    <option value="Dokter Spesialis">Dokter Spesialis</option>
                                                </select>
                                            </div>   				
                                        </div>
            
                                        <div class="col-md-12 mt-3"> 
                                            <div class="form-group">
                                                <label for="jenis-kegiatan" class="text-dark font-weight-bold">Jenis Kegiatan<span class="text-danger">*</span></label>
                                                <select class="form-select text-dark" id="jenis-kegiatan" name="jenis-kegiatan" aria-label="Jenis kegiatan">
                                                    <option selected>-- Pilih jenis kegiatan --</option>
                                                </select>
                                            </div>             
                                        </div>
                                        
                                        <div class="col-md-6 mt-3"> 
                                            <div class="form-group">
                                                <label for="gelar-depan" class="text-dark font-weight-bold">Gelar Depan</label>
                                                <input type="text" class="form-control" name="gelar-depan" id="gelar-depan" placeholder="Dr, Prof, Ir ..." autocomplete="off" >
                                            </div>             
                                        </div>
            
                                        <div class="col-md-6 mt-3 "> 
                                            <div class="form-group">
                                                <label for="" class="text-dark font-weight-bold">Gelar Belakang</label>
                                                <input type="text" class="form-control" name="gelar-belakang" id="gelar-belakang" placeholder="Sp.A, Sp.OG, ..." autocomplete="off" >
                                            </div>             
                                        </div>
            
                                        <div class="col-lg-12 mt-3 "> 
                                            <div class="form-group">
                                                <label for="" class="text-dark font-weight-bold">Nama Lengkap (Tanpa Gelar)<span class="text-danger">*</span></label>
                                                <input type="text" class="form-control " name="nama-lengkap-peserta" id="nama-lengkap-peserta" placeholder="Alexander Graham Bell ..." autocomplete="off" required>
                                            </div>   				
                                        </div>

                                        <div class="col-lg-12 mt-3 "> 
                                            <div class="form-group">
                                                <label for="nik" class="text-dark font-weight-bold">NIK (KTP)<span class="text-danger">*</span></label>
                                                <input type="text" class="form-control " name="nik" id="nik" placeholder="XXX..." autocomplete="off" required>
                                            </div>   				
                                        </div>
            
                                        <div class="col-lg-12 mt-3 "> 
                                            <div class="form-group">
                                                <label for="email" class="text-dark font-weight-bold">Email<span class="text-danger">*</span></label>
                                                <input type="text" class="form-control " name="email" id="email" placeholder="johndoe@mail.com" autocomplete="off" required>
                                            </div>   				
                                        </div>
            
                                        <div class="col-lg-12 mt-3 "> 
                                            <div class="form-group">
                                                <label for="nohp" class="text-dark font-weight-bold">No. Handphone / WA<span class="text-danger">*</span></label>
                                                <input type="text" class="form-control " name="nohp" id="nohp" placeholder="081234567890" autocomplete="off" required>
                                            </div>   				
                                        </div>
            
                                        <div class="col-lg-12 mt-3 "> 
                                            <div class="form-group">
                                                <label for="instansi-asal" class="text-dark font-weight-bold">Instansi Asal<span class="text-danger">*</span> </label>
                                                <input type="text" class="form-control " name="instansi-asal" id="instansi-asal" placeholder="Instasi ...." autocomplete="off" required>
                                            </div>   				
                                        </div>
            
                                        <div class="col-lg-12 mt-3 "> 
                                            <div class="form-group">
                                                <button type="submit" class="btn btn-primary">Submit Data</button>
                                                <button type="reset" class="btn btn-secondary">Reset</button>
            
                                            </div>   				
                                        </div>
                                        
                                </div>
                                    
                                    
                            </form>
                        </div>
                        
                        <div class="tab-pane fade" id="tab1-3">
                            <form action="#" id="form-uploadpembayaran">
                                <div class="row">
                                    <h2 class="mb-2 text-center">Upload Bukti Pembayaran</h2>
                                        @csrf
                                        <input type="hidden" name="id_pendaftaran" id="id_pendaftaran">
            
                                        <div class="col-lg-12 mt-3 "> 
                                            <div class="form-group">
                                                <label for="kode_invoice" class="text-dark font-weight-bold">Nomor Invoice Pendaftaran<span class="text-danger">*</span></label>
                                                <input type="text" class="form-control " name="kode_invoice" id="kode_invoice" placeholder="INV001.." autocomplete="off" required>
                                            </div>   				
                                        </div>

                                        <div id="table-datapeserta" class="col-lg-12 mt-3 d-none" > 
                                            <table class="table table-responsive table-bordered">
                                                <tr>
                                                    <th width="30%">Nama Lengkap</th>
                                                    <td id="nama-lengkap">-</td>
                                                </tr>
                                                <tr>
                                                    <th >No. Handphone / WA</th>
                                                    <td id="nohppeserta">-</td>
                                                </tr>
                                                <tr>
                                                    <th >Email</th>
                                                    <td id="emailpeserta">-</td>
                                                </tr>
                                                <tr>
                                                    <th >Jenis Peserta</th>
                                                    <td id="jenispeserta">-</td>
                                                </tr>
                                                <tr>
                                                    <th >Jenis Kegiatan</th>
                                                    <td id="jeniskegiatan">-</td>
                                                </tr>
                                                <tr>
                                                    <th >Total Biaya</th>
                                                    <td id="totalbiaya">-</td>
                                                </tr>
                                                
                                            </table>
                                        </div>

                                        <div class="col-lg-12 mt-3 "> 
                                            <div class="form-group">
                                                <label for="" class="text-dark font-weight-bold">Nama Lengkap </label>
                                                <input type="text" readonly class="bg-gray form-control text-dark " id="dtl-namalengkap">
                                            </div>   				
                                        </div>

                                        <div class="col-lg-12 mt-3 "> 
                                            <div class="form-group">
                                                <label for="" class="text-dark font-weight-bold">Jenis Kegiatan </label>
                                                <input type="text" readonly class="bg-gray form-control text-dark " id="dtl-jeniskegiatan">
                                            </div>   				
                                        </div>

                                        <div class="col-lg-12 mt-3 "> 
                                            <div class="form-group">
                                                <label for="" class="text-dark font-weight-bold">Total Biaya & Kode Unik</label>
                                                <input type="text" readonly class="bg-gray form-control text-dark " id="dtl-totalbiaya">
                                            </div>   				
                                        </div>

                                        <div class="col-lg-12 mt-3 "> 
                                            <div class="form-group">
                                                <label for="bukti-pembayaran" class="text-dark font-weight-bold">Bukti Transfer Pembayaran<span class="text-danger">*</span></label>
                                                <input type="file" class="form-control " name="bukti-pembayaran" id="bukti-pembayaran" accept="image/*" required>
                                                <small class="text-danger">*Format file .JPG, .JPEG, .PNG</small> <br>
                                                <small class="text-danger" style="margin-top: -10px;">*Maksimal besar file 1Mb</small> 
                                            </div>   				
                                        </div>

                                            
            
                                        <div class="col-lg-12 mt-3 "> 
                                            <div class="form-group">
                                                <button type="submit" class="btn btn-primary">Upload Bukti Pembayaran</button>
                                                <button type="reset" class="btn btn-secondary">Reset</button>
            
                                            </div>   				
                                        </div>
                                        
                                </div>
                            </form>
                        </div>
                        
                    </div>
                      
                    
                  </div>
                  
                </div>
              </section>
            
          </div>
        </div>
      </section>
      <!-- /section -->
    
    <!-- /.tab-content -->
@endsection