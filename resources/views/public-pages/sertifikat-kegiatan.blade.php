@extends('layout.mainbody')

@section('container')

    <section class="wrapper bg-soft-primary">
        <div class="container pt-10 pb-4 pt-md-14 pb-md-10 text-center">
        <div class="row">
            <div class="col-sm-10 col-md-8 col-lg-6 col-xl-6 col-xxl-5 mx-auto">
                <h1 class="display-1 mb-3">Sertifikat RiPiU 2024</h1>            
            </div>
            <!-- /column -->
        </div>
        <!-- /.row -->
        </div>
    </section>

        <section class="wrapper bg-primary">
            <div class="container py-14 py-md-16">
          {{-- <div class="row mb-3">
            <div class="col-md-10 col-lg-12 col-xl-10 col-xxl-9 mx-auto text-center">              
              <h3 class="display-4  px-lg-19 px-xl-18">Registrasi & Upload Bukti Pembayaran</h3>
            </div>
          </div> --}}
          <!--/.row -->
          <div class="row grid-view gx-md-8 gx-xl-10 gy-8 gy-lg-0">
            <section id="snippet-1" class="wrapper ">
                <div class="card">
                  <div class="card-body">

                    
                    <form action="#" id="form-cetaksertifikat">
                        <div class="row">
                            <h2 class="mb-2 text-center">Cetak Sertifikat Kegiatan</h2>
                                @csrf
    
                                <div class="col-lg-12 mt-3 "> 
                                    <div class="form-group">
                                        <label for="idpesertaripiu" class="text-dark font-weight-bold">Nomor ID Peserta<span class="text-danger">*</span></label>
                                        <input type="text" class="form-control " name="idpesertaripiu" id="idpesertaripiu" placeholder="RIPIU-0XXX" autocomplete="off" required autofocus>
                                    </div>   				
                                </div>

                                <div class="col-lg-12 mt-3 "> 
                                    <div class="form-group">
                                        <label for="" class="text-dark font-weight-bold">Nama Lengkap </label>
                                        <input type="text" class="form-control text-dark " name="namalengkappeserta" id="dtl-namalengkap" required>
                                    </div>   				
                                </div>

                                <div class="col-lg-12 mt-3 "> 
                                    <div class="form-group">
                                        <label for="" class="text-dark font-weight-bold">Jenis Kegiatan </label>
                                        <input type="text" readonly class="bg-gray form-control text-dark " id="dtl-jeniskegiatan">
                                    </div>   				
                                </div>
                                    
    
                                <div class="col-lg-12 mt-3 "> 
                                    <div class="form-group">
                                        <button type="submit" class="btn btn-primary disabled" id="btn-cetakserti">Cetak Sertifikat</button>
                                        <button type="reset" class="btn btn-secondary">Reset</button>
    
                                    </div>   				
                                </div>
                                
                        </div>
                    </form>

                    <div class="alert alert-danger alert-icon alert-dismissible fade show mt-5 d-none" id="notif-alertserti" role="alert">
                        <i class="uil uil-times-circle"></i> <span id="text-notif-alertserti"></span>
                    </div>

                    <div class="alert alert-success alert-icon alert-dismissible fade show mt-5 d-none" id="notif-alertsertisuccess" role="alert">
                        <i class="uil uil-check-circle"></i> <span id="text-notif-alertsertisuccess"></span>
                    </div>
                      
                    
                  </div>
                  
                </div>
              </section>
            
          </div>
        </div>
      </section>
      <!-- /section -->
    
    <!-- /.tab-content -->
@endsection