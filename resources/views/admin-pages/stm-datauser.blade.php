@extends('admin-pages.stm-mainbody')

@section('container')

    

    <div class="row">
        <div class="col-12">
            <div class="card-box">
                <h4 class="header-title">Data Paket Kegiatan</h4>
                <p class="sub-header">
                    {{-- See how aspects of the Bootstrap grid system work across multiple devices with a handy table. --}}
                </p>

                <button type="button" class="mb-4 btn btn-sm btn-primary waves-effect waves-light" data-toggle="modal" data-target=".modal-tambah-user">Tambah User</button>

                <div class="table-responsive">

                    <table id="table-datauser" class="table table-striped table-bordered mb-0" width="100%">
                        <thead>
                            <tr>
                                <th class="text-center">#</th>
                                <th class="text-center">Nama Lengkap</th>
                                <th class="text-center">Email</th>
                                <th class="text-center">Level</th>
                                <th class="text-center">Status</th>
                                <th class="text-center">Aksi</th>
                            </tr>
                        </thead>
                    </table>


                </div>
            </div>
        </div>
    </div>

    <div class="modal fade modal-tambah-user" id="modalTambahUser" tabindex="-1"  role="dialog" aria-labelledby="modalTambahUserLabel" aria-hidden="true">
        <div class="modal-dialog modal-lg">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="myExtraLargeModalLabel">Tambah User</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                </div>
                <div class="modal-body">
                    <div class="row">
                        <div class="col-xl-12">
                            <form id="form-tambah-user" method="post" action="#">
                                @csrf
                                <div class="form-group">
                                    <label for="nama_lengkap">Nama Lengkap</label>
                                    <input type="text" class="form-control" id="nama_lengkap" name="nama_lengkap" required placeholder="pamki member, trainee, other...">
                                </div>
                                <div class="form-group">
                                    <label for="email">Email</label>
                                    <input type="text" class="form-control" id="email" name="email" required placeholder="pamki member, trainee, other...">
                                </div>
                                <div class="form-group">
                                    <label for="password">Password</label>
                                    <input type="password" class="form-control" id="password" name="password" required >
                                </div>
                                <div class="form-group">
                                    <label for="no_hp">No. Hp</label>
                                    <input type="text" class="form-control" id="no_hp" name="no_hp" required >
                                </div>

                                <div class="form-group">
                                    <label for="asal_peserta">Level User</label>
                                    <select class="form-control" id="level_user" name="level_user" required>
                                        <option value="adm-pendaftaran">Adm Pendaftaran</option>
                                        <option value="adm-abstract">Adm Abstract</option>
                                        <option value="adm-hotel">Adm Hotel</option>
                                        <option value="admin">Administrator</option>
                                    </select>
                                </div>

                                <div class="form-group">
                                    <label for="status">Status Akun</label>
                                    <select class="form-control" id="status" name="status" required>
                                        <option value="aktif">Aktif</option>
                                        <option value="tidak-aktif">Tidak Aktif</option>
                                    </select>
                                </div>

                                <button type="submit" class="btn btn-primary">Tambah User</button>
                                <button type="reset" class="btn btn-warning">Reset</button>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    

    <div class="modal fade modal-edit-user" id="modalEditUser" tabindex="-1" role="dialog" aria-labelledby="modalEdituserLabel" aria-hidden="true">
        <div class="modal-dialog modal-lg">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="modalEdituserLabel">Edit User </h5>
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                </div>
                <div class="modal-body">
                    <div class="row">
                        <div class="col-xl-12">
                            <form id="form-edit-user" method="post" action="#">
                                @csrf
                                <input type="hidden" id="userid" name="userid">
                                <div class="form-group">
                                    <label for="nama_lengkap">Nama Lengkap</label>
                                    <input type="text" class="form-control" id="unama_lengkap" name="nama_lengkap" required placeholder="pamki member, trainee, other...">
                                </div>
                                <div class="form-group">
                                    <label for="email">Email</label>
                                    <input type="text" class="form-control" id="uemail" name="email" required placeholder="pamki member, trainee, other...">
                                </div>
                                <div class="form-group">
                                    <label for="password">Password</label>
                                    <input type="password" class="form-control" id="upassword" name="password"  >
                                </div>
                                <div class="form-group">
                                    <label for="no_hp">No. Hp</label>
                                    <input type="text" class="form-control" id="uno_hp" name="no_hp" required >
                                </div>

                                <div class="form-group">
                                    <label for="asal_peserta">Level User</label>
                                    <select class="form-control" id="ulevel_user" name="level_user" required>
                                        <option value="adm-pendaftaran">Adm Pendaftaran</option>
                                        <option value="adm-abstract">Adm Abstract</option>
                                        <option value="adm-hotel">Adm Hotel</option>
                                        <option value="admin">Administrator</option>
                                    </select>
                                </div>

                                <div class="form-group">
                                    <label for="status">Status Akun</label>
                                    <select class="form-control" id="ustatus" name="status" required>
                                        <option value="aktif">Aktif</option>
                                        <option value="tidak-aktif">Tidak Aktif</option>
                                    </select>
                                </div>

                                <button type="submit" class="btn btn-primary">Edit User</button>
                                <button type="reset" class="btn btn-warning">Reset</button>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

@endsection        