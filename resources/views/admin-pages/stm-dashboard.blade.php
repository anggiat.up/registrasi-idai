@extends('admin-pages.stm-mainbody')

@section('container')

    <div class="row">
        <div class="col-md-6 col-xl-4">
            <div class="card-box tilebox-one">
                <i class="icon-layers float-right m-0 h2 text-dark"></i>
                <h6 class="text-dark text-uppercase mt-0">Pendaftaran</h6>
                <h3 class="my-3"><span data-plugin="counterup">{{ $jumlah1 }}</span> Transaksi</h3>
            </div>
        </div>

        <div class="col-md-6 col-xl-4">
            <div class="card-box tilebox-one">
                <i class="icon-paypal float-right m-0 h2 text-dark"></i>
                <h6 class="text-dark text-uppercase mt-0">Menunggu Verifikasi</h6>
                <h3 class="my-3"><span data-plugin="counterup">{{ $jumlah2 }}</span> Transaksi</h3>
            </div>
        </div>

        <div class="col-md-6 col-xl-4">
            <div class="card-box tilebox-one">
                <i class="icon-chart float-right m-0 h2 text-dark"></i>
                <h6 class="text-dark text-uppercase mt-0">Peserta Terdaftar</h6>
                <h3 class="my-3"><span data-plugin="counterup">{{ $jumlah3 }}</span> Orang</h3>
            </div>
        </div>

        {{-- <div class="col-md-6 col-xl-3">
            <div class="card-box tilebox-one">
                <i class="icon-rocket float-right m-0 h2 text-dark"></i>
                <h6 class="text-dark text-uppercase mt-0">Lomba Abstract</h6>
                <h3 class="my-3"><span data-plugin="counterup">0</span> Orang</h3>
            </div>
        </div> --}}
    </div>

    <div class="row items-align-baseline text-dark">
            

        <div class="col-md-6 mb-6">
          <div class="card shadow">
            <div class="card-header">
              <strong class="card-title">DATA PESERTA REGISTRASI</strong>
            </div>
            <div class="card-body">
              <div class="list-group list-group-flush my-n3">
                <div class="list-group-item">
                  <div class="row align-items-center">
                    <div class="col text-dark">
                      <small><strong>Total Peserta</strong></small>
                      <div class="my-0 small">Peserta menunggu konfirmasi [{{ $jumlah2 }} Peserta]</div>
                      <div class="my-0 small">Peserta dikonfirmasi [{{ $jumlah4 }} Peserta]</div>
                      <div class="my-0 small">Peserta ditolak [{{ $jumlah5 }} Peserta]</div>
                    </div>
                    <div class="col-auto">
                      <small class=" text-dark"><b>{{ $jumlah1 }} Peserta</b></small>
                    </div>
                  </div>
                </div>
                <div class="list-group-item">
                  <div class="row align-items-center text-dark">
                    <div class="col">
                      <small><strong>Mahasiswa / Ko-Asisten</strong></small>
                      <div class="my-0 text-dark small">Peserta dikonfirmasi [{{ $mahakoasverif }} Peserta]</div>
                    </div>
                    <div class="col-auto">
                      <small class=" text-dark"><b>{{ $mahakoas }} Peserta</b></small>
                    </div>
                  </div>
                </div>
                <div class="list-group-item">
                  <div class="row align-items-center text-dark">
                    <div class="col">
                      <small><strong>Dokter Umum / PPDS</strong></small>
                      <div class="my-0 text-dark small">Peserta dikonfirmasi [{{ $umumppdsverif }} Peserta]</div>
                    </div>
                    <div class="col-auto">
                      <small class=" text-dark"><b>{{ $umumppds }} Peserta</b></small>
                    </div>
                  </div> <!-- / .row -->
                </div>
                <div class="list-group-item">
                  <div class="row align-items-center text-dark">
                    <div class="col">
                      <small><strong>Dokter Spesialis</strong></small>
                      <div class="my-0 text-dark small">Peserta dikonfirmasi [{{ $spesialis }} Peserta]</div>
                    </div>
                    <div class="col-auto">
                      <small class=" text-dark"><b>{{ $spesialisverif }} Peserta</b></small>
                    </div>
                  </div>
                </div> 
                                     
                <!-- / .row -->
              </div> <!-- / .list-group -->
            </div> <!-- / .card-body -->
          </div> <!-- / .card -->
        </div>


        <div class="col-md-6 mb-6">
          <div class="card shadow">
            <div class="card-header">
              <strong class="card-title">DATA PESERTA TERKONFIRMASI</strong>
            </div>
            <div class="card-body">
              <div class="list-group list-group-flush my-n3">
                <div class="list-group-item">
                  <div class="row align-items-center">
                    <div class="col">
                      <small><strong>SIMPOSIUM</strong></small>
                    </div>
                    <div class="col-auto">
                      <small class=" text-dark"><b>{{ $symposium }} Peserta</b></small>
                    </div>
                  </div>
                </div>
                <div class="list-group-item">
                  <div class="row align-items-center">
                    <div class="col">
                      <small><strong>ERIA + NEUROPEDIATRI</strong></small>
                    </div>
                    <div class="col-auto">
                      <small class=" text-dark"><b>{{ $ws1 }} Peserta</b></small>
                    </div>
                  </div>
                </div>
                <div class="list-group-item">
                  <div class="row align-items-center">
                    <div class="col">
                      <small><strong>NEONATOLOGI</strong></small>
                    </div>
                    <div class="col-auto">
                      <small class=" text-dark"><b>{{ $ws2 }} Peserta</b></small>
                    </div>
                  </div> <!-- / .row -->
                </div>
                <div class="list-group-item">
                  <div class="row align-items-center">
                    <div class="col">
                      <small><strong>GASTROHEPATOLOGI + BEDAH ANAK</strong></small>
                    </div>
                    <div class="col-auto">
                      <small class=" text-dark"><b>{{ $ws3 }} Peserta</b></small>
                    </div>
                  </div>
                </div> 
                <div class="list-group-item">
                  <div class="row align-items-center">
                    <div class="col">
                      <small><strong>HEMATO-ONKOLOGI</strong></small>
                    </div>
                    <div class="col-auto">
                      <small class=" text-dark"><b>{{ $ws4 }} Peserta</b></small>
                    </div>
                  </div>
                </div>                     
                <!-- / .row -->
              </div> <!-- / .list-group -->
            </div> <!-- / .card-body -->
          </div> <!-- / .card -->
        </div>
    </div> 
    @if (auth()->user()->level_akun == 'adm-pendaftaran' || auth()->user()->level_akun == 'admin')
    
    <div class="row">
        <div class="col-12">
            <div class="card-box">
                <h4 class="header-title">Tabel Data Pendaftaran Peserta</h4>
                <p class="sub-header">
                    {{-- See how aspects of the Bootstrap grid system work across multiple devices with a handy table. --}}
                </p>

                <div class="table-responsive">
                    <table id="table-pendaftaran" class="table table-striped table-bordered mb-0" width="100%">
                        <thead>
                            <tr>
                                <th class="text-center">#</th>
                                <th class="text-center">Invoice</th>
                                <th class="text-center">Kode Unique</th>
                                <th class="text-center">Total Biaya</th>
                                <th class="text-center">Waktu Pendaftarn</th>
                                <th class="text-center">Status</th>
                                <th class="text-center">Notifikasi <br>Pembayaran | Pendaftaran</th>
                                <th class="text-center">File Bukti</th>
                                <th class="text-center">Aksi</th>
                            </tr>
                        </thead>                       
                    </table>
                </div>
            </div>
        </div>
    </div>

    {{-- Modal Bukti Pembayaran --}}
    <div class="modal fade modalBuktiPembayaran" id="modalBuktiPembayaran" tabindex="-1" role="dialog" aria-labelledby="modalPembayaranLabel" aria-hidden="true" style="display: none;">
        <div class="modal-dialog modal-lg">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="modalPembayaranLabel">File Bukti Pembayaran Peserta</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                </div>
                <div class="modal-body">
                    <img id="file-buktipembayaran" src="" width="100%" alt="">
                     <hr class="mt-2 mb-2">
                    <div class="table-responsive">
                        <table class="table table-nowrap">
                                <tr>
                                    <th width="30%">NOMOR INVOICE</th>
                                    <td width="5%">:</td>
                                    <td id="tf-invoice"></td>
                                </tr>
                                <tr>
                                    <th>JUMLAH PEMBAYARAN</th>
                                    <td>:</td>
                                    <td id="tf-total"></td>
                                </tr>
                                <tr>
                                    <th>KODE UNIQ</th>
                                    <td>:</td>
                                    <td id="tf-unik"></td>
                                </tr>
                                <tr>
                                    <th class="text-center" colspan="3" id="tf-buttonver">
                                        
                                    </th>
                                </tr>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>

    {{-- Modal Detail Peserta --}}
    <div class="modal fade modalDetailPeserta" id="modalDetailPeserta" tabindex="-1" role="dialog" aria-labelledby="modalDetailPesertaLabel" aria-hidden="true" style="display: none;">
        <div class="modal-dialog modal-lg">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="modalDetailPesertaLabel">Detail Data Peserta P</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                </div>
                <div class="modal-body">
                    <div class="table-responsive">
                        <table class="table table-nowrap">
                                <tr>
                                    <td colspan="3" class="bg-secondary text-white text-center"> <i>Data Pendaftaran</i></td>
                                </tr>
                                <tr>
                                    <th width="30%">NOMOR INVOICE</th>
                                    <td width="5%">:</td>
                                    <td id="md-invoice"></td>
                                </tr>
                                <tr>
                                    <th>TOTAL PEMBAYARAN</th>
                                    <td>:</td>
                                    <td id="md-total"></td>
                                </tr>
                                <tr>
                                    <th>KODE UNIQ</th>
                                    <td>:</td>
                                    <td id="md-unik"></td>
                                </tr>
                                <tr>
                                    <th>WAKTU PENDAFTARAN</th>
                                    <td>:</td>
                                    <td id="md-waktu"></td>
                                </tr>
                                <tr>
                                    <th>STATUS PEMBAYARAN</th>
                                    <td>:</td>
                                    <td id="md-status"></td>
                                </tr>
                                <tr>
                                    <th>DIVERIFIKASI OLEH</th>
                                    <td>:</td>
                                    <td id="md-admin"></td>
                                </tr>
                                <tr>
                                    <td colspan="3" class="bg-secondary text-white text-center"> <i>Data Peserta</i></td>
                                </tr>
                                <tr>
                                    <td colspan="3" id="md-tablepeserta">
                                        
                                    </td>
                                </tr>
                                
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
    @endif
@endsection        