<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8" />
        <title>{{ $nama_halaman }} | RiPiU3 2024</title>
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta content="Responsive bootstrap 4 admin template" name="description" />
        <meta content="Coderthemes" name="author" />
        <meta http-equiv="X-UA-Compatible" content="IE=edge" />
        <!-- App favicon -->
        <link rel="shortcut icon" href="{{ asset('sistems/images/favicon.ico') }}">
        <meta name="csrf-token" content="{{ csrf_token() }}">

        <!-- Table datatable css -->
        <link href="{{ asset('sistems//libs/datatables/dataTables.bootstrap4.min.css') }}" rel="stylesheet" type="text/css" />
        
        <link href="{{ asset('sistems/libs/datatables/buttons.bootstrap4.min.css')}}" rel="stylesheet" type="text/css" />
        <link href="{{ asset('sistems/libs/datatables/responsive.bootstrap4.min.css')}}" rel="stylesheet" type="text/css" />
        <link href="{{ asset('sistems/libs/datatables/select.bootstrap4.min.css')}}" rel="stylesheet" type="text/css" />

        <!-- Magnific -->
        <link rel="stylesheet" href="{{ asset('sistems/libs/magnific-popup/magnific-popup.css') }}"/>

        <!-- Sweet Alert-->
        <link href="{{ asset('sistems/libs/sweetalert2/sweetalert2.min.css') }}" rel="stylesheet" type="text/css" />

        <!-- Notification css (Toastr) -->
        <link href="{{ asset('sistems/libs/toastr/toastr.min.css') }}" rel="stylesheet" type="text/css" />

        <!-- App css -->
        <link href="{{ asset('sistems/css/bootstrap.min.css') }}" rel="stylesheet" type="text/css" id="bootstrap-stylesheet" />
        <link href="{{ asset('sistems/css/icons.min.css') }}" rel="stylesheet" type="text/css" />
        <link href="{{ asset('sistems/css/app.min.css') }}" rel="stylesheet" type="text/css"  id="app-stylesheet" />

    </head>

    <body>

        <!-- Begin page -->
        <div id="wrapper">
        <div id="base-url" data-baseurl="{{ url('') }}" ></div>
        <div id="loading-ajax">Loading...</div>      
        <style>
            #loading-ajax{
                width: 100%;
                height: 100%;
                position: fixed;
                text-indent: 100%;
                margin-top: 0px;
                background: #e0e0e0 url("{{ asset('img/loading2.gif') }}") no-repeat center;
                z-index: 1321050 ;
                opacity: 0.6;
                background-size: 20%;
            }
        </style>

            
            <!-- Topbar Start -->
            <div class="navbar-custom">
                <ul class="list-unstyled topnav-menu float-right mb-0">

                    <li class="dropdown notification-list">
                        <a class="nav-link dropdown-toggle  waves-effect waves-light" data-toggle="dropdown" href="#" role="button" aria-haspopup="false" aria-expanded="false">
                            <i class="mdi mdi-bell-outline noti-icon"></i>
                            <span class="noti-icon-badge"></span>
                        </a>
                        <div class="dropdown-menu dropdown-menu-right dropdown-lg">

                            <!-- item-->
                            <div class="dropdown-item noti-title">
                                <h5 class="font-16 text-white m-0">
                                    <span class="float-right">
                                        <a href="" class="text-white">
                                            <small>Clear All</small>
                                        </a>
                                    </span>Notification
                                </h5>
                            </div>

                            <div class="slimscroll noti-scroll">

                                <!-- item-->
                                <a href="javascript:void(0);" class="dropdown-item notify-item">
                                    <div class="notify-icon bg-success">
                                        <i class="mdi mdi-settings-outline"></i>
                                    </div>
                                    <p class="notify-details">New settings
                                        <small class="text-muted">There are new settings available</small>
                                    </p>
                                </a>
                    
                                <!-- item-->
                                <a href="javascript:void(0);" class="dropdown-item notify-item">
                                    <div class="notify-icon bg-info">
                                        <i class="mdi mdi-bell-outline"></i>
                                    </div>
                                    <p class="notify-details">Updates
                                        <small class="text-muted">There are 2 new updates available</small>
                                    </p>
                                </a>

                                <!-- item-->
                                <a href="javascript:void(0);" class="dropdown-item notify-item">
                                    <div class="notify-icon bg-danger">
                                        <i class="mdi mdi-account-plus"></i>
                                    </div>
                                    <p class="notify-details">New user
                                        <small class="text-muted">You have 10 unread messages</small>
                                    </p>
                                </a>

                                <!-- item-->
                                <a href="javascript:void(0);" class="dropdown-item notify-item">
                                    <div class="notify-icon bg-info">
                                        <i class="mdi mdi-comment-account-outline"></i>
                                    </div>
                                    <p class="notify-details">Caleb Flakelar commented on Admin
                                        <small class="text-muted">4 days ago</small>
                                    </p>
                                </a>

                                <!-- item-->
                                <a href="javascript:void(0);" class="dropdown-item notify-item">
                                    <div class="notify-icon bg-secondary">
                                        <i class="mdi mdi-heart"></i>
                                    </div>
                                    <p class="notify-details">Carlos Crouch liked
                                        <b>Admin</b>
                                        <small class="text-muted">13 days ago</small>
                                    </p>
                                </a>
                            </div>

                            <!-- All-->
                            <a href="javascript:void(0);" class="dropdown-item text-primary notify-item notify-all">
                                View all
                                <i class="fi-arrow-right"></i>
                            </a>

                        </div>
                    </li>

                    <li class="dropdown notification-list">
                        <a class="nav-link dropdown-toggle nav-user mr-0 waves-effect waves-light" data-toggle="dropdown" href="#" role="button" aria-haspopup="false" aria-expanded="false">
                            <img src="{{ asset('sistems/images/users/avatar-1.jpg') }}" alt="user-image" class="rounded-circle">
                            <span class="d-none d-sm-inline-block ml-1 font-weight-medium">{{ auth()->user()->nama_lengkap; }}</span>
                            <i class="mdi mdi-chevron-down d-none d-sm-inline-block"></i>
                        </a>
                        <div class="dropdown-menu dropdown-menu-right profile-dropdown ">
                            <!-- item-->
                            <div class="dropdown-header noti-title">
                                <h6 class="text-overflow text-white m-0">Welcome !</h6>
                            </div>

                            <!-- item-->
                            {{-- <a href="javascript:void(0);" class="dropdown-item notify-item">
                                <i class="mdi mdi-account-outline"></i>
                                <span>Profile</span>
                            </a> --}}

                      

                            <div class="dropdown-divider"></div>

                            <!-- item-->
                            <a href="javascript:void(0);" id="logout-btn" data-link="{{ url('logout') }}" class="dropdown-item notify-item">
                                <i class="mdi mdi-logout-variant"></i>
                                <span>Logout</span>
                            </a>

                        </div>
                    </li>


                </ul>

                <!-- LOGO -->
                <div class="logo-box">
                    <a href="{{ url('sistem/dashboard') }}" class="logo text-center logo-dark">
                        <span class="logo-lg">
                            <img src="{{asset('sistems/images/logo-dark.png')}}" alt="" height="42">
                            <!-- <span class="logo-lg-text-dark">Uplon</span> -->
                        </span>
                        <span class="logo-sm">
                            <!-- <span class="logo-lg-text-dark">U</span> -->
                            <img src="{{asset('sistems/images/logo-sm.png')}}" alt="" height="24">
                        </span>
                    </a>

                    <a href="{{ url('sistem/dashboard') }}" class="logo text-center logo-light">
                        <span class="logo-lg">
                            <img src="{{asset('sistems/images/logo-light.')}}png" alt="" height="22">
                            <!-- <span class="logo-lg-text-dark">Uplon</span> -->
                        </span>
                        <span class="logo-sm">
                            <!-- <span class="logo-lg-text-dark">U</span> -->
                            <img src="{{asset('sistems/images/logo-sm-lig')}}ht.png" alt="" height="24">
                        </span>
                    </a>
                </div>

                <ul class="list-unstyled topnav-menu topnav-menu-left m-0">
                    <li>
                        <button class="button-menu-mobile waves-effect waves-light">
                            <i class="mdi mdi-menu"></i>
                        </button>
                    </li>
        
                 

                </ul>
            </div>
            <!-- end Topbar -->

            
            <!-- ========== Left Sidebar Start ========== -->
            <div class="left-side-menu">

                <div class="slimscroll-menu">

                    <!--- Sidemenu -->
                    <div id="sidebar-menu">

                        <ul class="metismenu" id="side-menu">

                            <li class="menu-title">Navigation</li>

                            <li>
                                <a href="{{ url('sistem/dashboard') }}">
                                    <i class="mdi mdi-view-dashboard"></i>
                                    <span> Dashboard </span>
                                </a>
                            </li>
                            @if (auth()->user()->level_akun == 'adm-pendaftaran' || auth()->user()->level_akun == 'admin')
                            <li>
                                <a href="{{ url('sistem/pendaftaran-peserta') }}">
                                    <i class="mdi mdi-account-multiple-plus-outline"></i>
                                    <span> Pendaftaran Peserta </span>
                                </a>
                            </li>
                            <li>
                                <a href="{{ url('sistem/peserta-terverifikasi') }}">
                                    <i class="mdi mdi-account-box-multiple-outline"></i>
                                    <span> Peserta Terverifikasi </span>
                                </a>
                            </li>
                            @endif
                            

                            @if (auth()->user()->level_akun == 'admin')                                
                            <li class="menu-title mt-2">Admin Fitur</li>

                            <li>
                                <a href="{{ url('sistem/paket-kegiatan') }}">
                                    <i class="mdi mdi-book-multiple"></i>
                                    <span> Daftar Paket </span>
                                </a>
                            </li>

                            
                            <li>
                                <a href="{{ url('sistem/data-user') }}">
                                    <i class="mdi mdi-account-key"></i>
                                    <span> Data User </span>
                                </a>
                            </li>
                            @endif


                          
                        </ul>

                    </div>
                    <!-- End Sidebar -->

                    <div class="clearfix"></div>

                </div>
                <!-- Sidebar -left -->

            </div>
            <!-- Left Sidebar End -->

            <!-- ============================================================== -->
            <!-- Start Page Content here -->
            <!-- ============================================================== -->

            <div class="content-page">
                <div class="content">
                    
                    <!-- Start Content-->
                    <div class="container-fluid">
                        

                        <!-- start page title -->
                        <div class="row">
                            <div class="col-12">
                                <div class="page-title-box">
                                    <div class="page-title-right">
                                        <ol class="breadcrumb m-0">
                                            <li class="breadcrumb-item"><a href="javascript: void(0);">RiPiU3 2024</a></li>
                                            <li class="breadcrumb-item active">{{ $nama_halaman }}</li>
                                        </ol>
                                    </div>
                                    <h4 class="page-title">{{ $nama_halaman }}</h4>
                                </div>
                            </div>
                        </div>     
                        <!-- end page title --> 

                        @yield('container')


                        
                    </div> <!-- end container-fluid -->

                </div> <!-- end content -->

                

                <!-- Footer Start -->
                <footer class="footer">
                    <div class="container-fluid">
                        <div class="row">
                            <div class="col-md-12">
                                2024 &copy; RiPiU3 2024 by <a href="">Narapati</a>
                            </div>
                        </div>
                    </div>
                </footer>
                <!-- end Footer -->

            </div>

            <!-- ============================================================== -->
            <!-- End Page content -->
            <!-- ============================================================== -->

        </div>
        <!-- END wrapper -->

        

        <!-- Right bar overlay-->
        <div class="rightbar-overlay"></div>

        

        <!-- Vendor js -->
        <script src="{{ asset('sistems/js/vendor.min.js') }}"></script>

        <!-- Modal-Effect -->
        <script src="{{ asset('sistems/libs/custombox/custombox.min.js') }}"></script>

        <!--Morris Chart-->
        <script src="{{ asset('sistems/libs/morris-js/morris.min.js') }}"></script>
        <script src="{{ asset('sistems/libs/raphael/raphael.min.js') }}"></script>

        <!-- Dashboard init js-->
        <script src="{{ asset('sistems/js/pages/dashboard.init.js') }}"></script>

        <!-- Sweet Alerts js -->
        <script src="{{ asset('sistems/libs/sweetalert2/sweetalert2.min.js') }}"></script>

        <!-- Sweet alert init js-->
        <script src="{{ asset('sistems/js/pages/sweet-alerts.init.js') }}"></script>

        <!-- Datatable plugin js -->
        <script src="{{asset('sistems/libs/datatables/jquery.dataTables.min.js')}}"></script>
        <script src="{{asset('sistems/libs/datatables/dataTables.bootstrap4.min.js')}}"></script>

        <script src="{{asset('sistems/libs/datatables/dataTables.responsive.min.js')}}"></script>
        <script src="{{asset('sistems/libs/datatables/responsive.bootstrap4.min.js')}}"></script>

        <script src="{{asset('sistems/libs/datatables/dataTables.buttons.min.js')}}"></script>
        <script src="{{asset('sistems/libs/datatables/buttons.bootstrap4.min.js')}}"></script>

        <script src="{{asset('sistems/libs/jszip/jszip.min.js')}}"></script>
        <script src="{{asset('sistems/libs/pdfmake/pdfmake.min.js')}}"></script>
        <script src="{{asset('sistems/libs/pdfmake/vfs_fonts.js')}}"></script>

        <script src="{{asset('sistems/libs/datatables/buttons.html5.min.js')}}"></script>
        <script src="{{asset('sistems/libs/datatables/buttons.print.min.js')}}"></script>

        <script src="{{asset('sistems/libs/datatables/dataTables.keyTable.min.js')}}"></script>
        <script src="{{asset('sistems/libs/datatables/dataTables.select.min.js')}}"></script>

        <!-- Toastr js -->
        <script src="{{ asset('sistems/libs/toastr/toastr.min.js') }}"></script>

        <script src="{{ asset('sistems/js/pages/toastr.init.js') }}"></script>

        <!-- Datatables init -->
        <script src="{{asset('sistems/js/pages/datatables.init.js')}}"></script>

        <!-- isotope filter plugin -->
        <script src="{{ asset('sistems/libs/isotope/isotope.pkgd.min.js')}}"></script>

        <!-- Magnific -->
        <script src="{{ asset('sistems/libs/magnific-popup/jquery.magnific-popup.min.js')}}"></script>

        <!-- Gallery Init-->
        <script src="{{ asset('sistems/js/pages/gallery.init.js')}}"></script>

        <!-- App js -->
        <script src="{{ asset('sistems/js/app.min.js?v='.filemtime(public_path('sistems/js/app.min.js'))) }}"></script>  
        <script src="{{ asset('sistems/js/customs.js?v='.filemtime(public_path('sistems/js/customs.js'))) }}"></script>  
        <script src="{{ asset('sistems/js/customs-datatables.js?v='.filemtime(public_path('sistems/js/customs-datatables.js'))) }}"></script>  
        
    </body>
</html>