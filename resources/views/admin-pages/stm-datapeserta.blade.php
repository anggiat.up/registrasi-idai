@extends('admin-pages.stm-mainbody')

@section('container')

    

    <div class="row">
        <div class="col-12">
            <div class="card-box">
                <h4 class="header-title">Table Data Peserta</h4>
                <p class="sub-header">
                    {{-- See how aspects of the Bootstrap grid system work across multiple devices with a handy table. --}}
                </p>
                
                <div class="row">
                    <div class="col-md-5">
                        <select name="jenis-laporan" id="jenis-laporan" class="form-control">
                            <option value="Semua">Semua Data</option>
                            <option value="Symposium">Symposium</option>
                            <option value="Workshop 1">Workshop 1</option>
                            <option value="Workshop 2">Workshop 2</option>
                            <option value="Workshop 3">Workshop 3</option>
                            <option value="Workshop 4">Workshop 4</option>
                        </select>
                    </div>
                    <div class="col-md-2">
                        <button id="btn-cetaklaporan" class="mb-4 btn  btn-primary waves-effect waves-light" >Cetak Laporan</button>
                    </div>
                </div>
                
                <div class="table-responsive">
                    
                    <table id="table-peserta" class="table table-striped table-bordered mb-0" width="100%">
                        <thead>
                        <tr>
                            <th class="text-center">#</th>
                            <th class="text-center">ID Peserta</th>
                            <th class="text-center">Invoice</th>
                            <th class="text-center">Nama Lengkap</th>
                            <th class="text-center">Jenis Peserta</th>
                            <th class="text-center">Nama Paket</th>
                            <th class="text-center">Notifikasi <br>Pembayaran | Pendaftaran</th>
                            <th class="text-center">Aksi</th>
                        </tr>
                        </thead>                       
                    </table>


                </div>
            </div>
        </div>
    </div>

    <div class="modal fade modalBuktiPembayaran" id="modalBuktiPembayaran" tabindex="-1" role="dialog" aria-labelledby="modalPembayaranLabel" aria-hidden="true" style="display: none;">
        <div class="modal-dialog modal-lg">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="modalPembayaranLabel">File Bukti Pembayaran Peserta</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                </div>
                <div class="modal-body">
                    <img id="file-buktipembayaran" src="" width="100%" alt="">
                     <hr class="mt-2 mb-2">
                    <div class="table-responsive">
                        <table class="table table-nowrap">
                                <tr>
                                    <th width="30%">NOMOR INVOICE</th>
                                    <td width="5%">:</td>
                                    <td id="tf-invoice"></td>
                                </tr>
                                <tr>
                                    <th>JUMLAH PEMBAYARAN</th>
                                    <td>:</td>
                                    <td id="tf-total"></td>
                                </tr>
                                <tr>
                                    <th>KODE UNIQ</th>
                                    <td>:</td>
                                    <td id="tf-unik"></td>
                                </tr>
                                <tr>
                                    <th class="text-center" colspan="3" id="tf-buttonver">
                                        
                                    </th>
                                </tr>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="modal fade modalDetailPeserta" id="modalDetailPeserta" tabindex="-1" role="dialog" aria-labelledby="modalDetailPesertaLabel" aria-hidden="true" style="display: none;">
        <div class="modal-dialog modal-lg">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="modalDetailPesertaLabel">Detail Data Peserta P</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                </div>
                <div class="modal-body">
                    <div class="table-responsive">
                        <table class="table table-nowrap">
                                <tr>
                                    <td colspan="3" class="bg-secondary text-white text-center"> <i>Data Pendaftaran</i></td>
                                </tr>
                                <tr>
                                    <th width="30%">NOMOR INVOICE</th>
                                    <td width="5%">:</td>
                                    <td id="md-invoice"></td>
                                </tr>
                                <tr>
                                    <th>TOTAL PEMBAYARAN</th>
                                    <td>:</td>
                                    <td id="md-total"></td>
                                </tr>
                                <tr>
                                    <th>KODE UNIQ</th>
                                    <td>:</td>
                                    <td id="md-unik"></td>
                                </tr>
                                <tr>
                                    <th>WAKTU PENDAFTARAN</th>
                                    <td>:</td>
                                    <td id="md-waktu"></td>
                                </tr>
                                <tr>
                                    <th>STATUS PEMBAYARAN</th>
                                    <td>:</td>
                                    <td id="md-status"></td>
                                </tr>
                                <tr>
                                    <th>DIVERIFIKASI OLEH</th>
                                    <td>:</td>
                                    <td id="md-admin"></td>
                                </tr>
                                <tr>
                                    <td colspan="3" class="bg-secondary text-white text-center"> <i>Data Peserta</i></td>
                                </tr>
                                <tr>
                                    <td colspan="3" id="md-tablepeserta">
                                        
                                    </td>
                                </tr>
                                
                        </table>
                    </div>                    
                </div>
            </div>
        </div>
    </div>
@endsection        