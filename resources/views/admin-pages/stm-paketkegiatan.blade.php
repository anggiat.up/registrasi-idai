@extends('admin-pages.stm-mainbody')

@section('container')

    

    <div class="row">
        <div class="col-12">
            <div class="card-box">
                <h4 class="header-title">Data Paket Kegiatan</h4>
                <p class="sub-header">
                    {{-- See how aspects of the Bootstrap grid system work across multiple devices with a handy table. --}}
                </p>

                <button type="button" class="mb-4 btn btn-sm btn-primary waves-effect waves-light" data-toggle="modal" data-target=".modal-tambah-paket">Tambah Paket</button>

                <div class="table-responsive">

                    <table id="table-paket" class="table table-striped table-bordered mb-0" width="100%">
                        <thead>
                        <tr>
                            <th class="text-center">#</th>
                            <th class="text-center">Jenis Peserta</th>
                            <th class="text-center">Nama Paket</th>
                            <th class="text-center">Harga</th>
                            <th class="text-center">Status</th>
                            <th class="text-center">Waktu</th>
                            <th class="text-center">Aksi</th>
                        </tr>
                        </thead>
                        {{-- <tbody>
                            <tr>
                                <th class="text-center align-middle" scope="row">1</th>
                                <td class="align-middle">Nasional</td>
                                <td class="align-middle">Specialis PAMKI Member</td>
                                <td class="align-middle">Syposium</td>
                                <td class="align-middle text-right">Rp. 1,000,000.-</td>
                                <td class="align-middle text-center"><span class="badge badge-success p-2">active</span></td>
                                <td class="align-middle text-center">
                                    <i class="fas fa-pen btn btn-sm btn-primary p-2"></i>
                                </td>
                            </tr>
                        </tbody> --}}
                    </table>


                </div>
            </div>
        </div>
    </div>

    <div class="modal fade modal-tambah-paket" id="modalTambahPaket" tabindex="-1"  role="dialog" aria-labelledby="modalTambahPaketLabel" aria-hidden="true">
        <div class="modal-dialog modal-lg">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="myExtraLargeModalLabel">Tambah Paket Kegiatan</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                </div>
                <div class="modal-body">
                    <div class="row">
                        <div class="col-xl-12">
                            <form id="form-tambah-paket" method="post" action="#">
                                @csrf
                               
                                <div class="form-group">
                                    <label for="jenis_peserta">Jenis Peserta</label>
                                    <input type="text" class="form-control" id="jenis_peserta" name="jenis_peserta" required placeholder="pamki member, trainee, other...">
                                </div>
                                <div class="form-group">
                                    <label for="nama_paket">Nama Paket</label>
                                    <input type="text" class="form-control" id="nama_paket" name="nama_paket" required placeholder="symposium, workshop...">
                                </div>
                                <div class="form-group">
                                    <label for="harga_paket">Harga Paket</label>
                                    <input type="text" class="form-control" id="harga_paket" name="harga_paket" required >
                                </div>

                                <div class="form-group">
                                    <label for="status">Status Paket</label>
                                    <select class="form-control" id="status" name="status" required>
                                        <option value="Active">Aktif</option>
                                        <option value="Inactive">Tidak Aktif</option>
                                    </select>
                                </div>


                                <button type="submit" class="btn btn-primary">Tambah Paket</button>
                                <button type="reset" class="btn btn-warning">Reset</button>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    

    <div class="modal fade modal-edit-paket" id="modalEditPaket" tabindex="-1" role="dialog" aria-labelledby="modalEditPaketLabel" aria-hidden="true">
        <div class="modal-dialog modal-lg">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="modalEditPaketLabel">Edit Paket Kegiatan</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                </div>
                <div class="modal-body">
                    <div class="row">
                        <div class="col-xl-12">
                            <form id="form-edit-paket" method="post" action="#">
                                @csrf
                                <input type="hidden" id="pketid" name="pketid">
                                
                                <div class="form-group">
                                    <label for="jenis_peserta">Jenis Peserta</label>
                                    <input type="text" class="form-control" id="ujenis_peserta" name="jenis_peserta" required placeholder="pamki member, trainee, other...">
                                </div>
                                <div class="form-group">
                                    <label for="nama_paket">Nama Paket</label>
                                    <input type="text" class="form-control" id="unama_paket" name="nama_paket" required placeholder="symposium, workshop...">
                                </div>
                                <div class="form-group">
                                    <label for="harga_paket">Harga Paket</label>
                                    <input type="number" class="form-control" id="uharga_paket" name="harga_paket" required >
                                </div>

                                <div class="form-group">
                                    <label for="status">Status Paket</label>
                                    <select class="form-control" id="ustatus" name="status" required>
                                        <option value="Active">Aktif</option>
                                        <option value="Inactive">Tidak Aktif</option>
                                    </select>
                                </div>

                                <button type="submit" class="btn btn-primary">Edit Paket</button>
                                <button type="reset" class="btn btn-warning">Reset</button>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

@endsection        