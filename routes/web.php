<?php

use App\Http\Controllers\AuthsController;
use App\Http\Controllers\RegistrasiIdaiController;
use App\Http\Controllers\Sistems\ManajemenController;
use Illuminate\Support\Facades\Route;
use Illuminate\Support\Facades\Response;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider and all of them will
| be assigned to the "web" middleware group. Make something great!
|
*/



Route::get('/', [RegistrasiIdaiController::class, 'pageDashboard'])->name('dashboard');
Route::get('/home', [RegistrasiIdaiController::class, 'pageDashboard'])->name('dashboard');
Route::get('/kontak-kami', [RegistrasiIdaiController::class, 'pageKontak'])->name('kontak-kami');
Route::get('/registrasi', [RegistrasiIdaiController::class, 'pageRegistrasi'])->name('registrasi');
Route::get('/sertifikat', [RegistrasiIdaiController::class, 'pageSertifikat'])->name('sertifikat');
Route::get('/sistem/harga-paket', [ManajemenController::class, 'reqAjaxHargaPaket'])->name('sistem.harga-paket');
Route::post('/registrasi-peserta', [ManajemenController::class, 'reqAjaxRegistrasiPeserta'])->name('registrasi-peserta');
Route::get('/detail-invoice', [ManajemenController::class, 'reqAjaxInvoice'])->name('detail-invoice');
Route::post('/upload-bukti-bayar', [ManajemenController::class, 'reqAjaxFormUploadPembayaran'])->name('upload-bukti-bayar');

Route::get('/detail-peserta', [ManajemenController::class, 'reqAjaxPstSertifikat'])->name('detail-peserta');
Route::post('/cetak-sertifikat', [ManajemenController::class, 'reqAjaxCetakSertifikat'])->name('cetak-sertifikat');

Route::get('/download/{filename}', function ($filename) {
    $filePath = public_path('generate-sertifikat/' . $filename);

    if (file_exists($filePath)) {
        return Response::download($filePath, $filename);
    } else {
        abort(404, 'File not found.');
    }
})->name('downloadFile');


Route::get('/pembayaran', [ManajemenController::class, 'template'])->name('pembayaran');
// Route::get('/pembayaran', function () { return view('layout/mail-notifikasipembayaran'); });



Route::get('/sistem', [AuthsController::class, 'index'])->name('sistem');
Route::get('/sistem/login', [AuthsController::class, 'index'])->name('sistem.login');
Route::get('/sistem/login', [AuthsController::class, 'index'])->name('login');
Route::post('/sistem/login', [AuthsController::class, 'AjaxprosesLogin'])->name('sistem.login');
Route::get('/logout', [AuthsController::class, 'logout'])->name('logout');

Route::middleware(['auth'])->group(function () {
    Route::get('/sistem/dashboard', [ManajemenController::class, 'pgeDashboard'])->name('sistem.dashboard');
    Route::get('/sistem/pendaftaran-peserta', [ManajemenController::class, 'pgePendaftaranPeserta'])->name('sistem.pendaftaran-peserta');
    Route::get('/sistem/peserta-terverifikasi', [ManajemenController::class, 'pgePesertaTerverifikasi'])->name('sistem.peserta-terverifikasi');
    Route::get('/sistem/paket-kegiatan', [ManajemenController::class, 'pgePaketKegiatan'])->name('sistem.paket-kegiatan');
    Route::get('/sistem/data-user', [ManajemenController::class, 'pgeDataUser'])->name('sistem.data-user');



    Route::get('/sistem/table-data-paket', [ManajemenController::class, 'reqAjaxTableDataPaket'])->name('sistem.table-data-paket');
    Route::get('/sistem/table-data-pendaftaran', [ManajemenController::class, 'reqAjaxTablePendaftaranPeserta'])->name('sistem.table-data-pendaftaran');
    Route::get('/sistem/table-data-peserta', [ManajemenController::class, 'reqAjaxTablePeserta'])->name('sistem.table-data-peserta');
    Route::get('/sistem/table-data-users', [ManajemenController::class, 'reqAjaxTableDataUser'])->name('sistem.table-data-users');

    Route::post('/sistem/tambah-paket', [ManajemenController::class, 'reqAjaxFormTambahPaket'])->name('sistem.tambah-paket');
    Route::get('/sistem/data-paket', [ManajemenController::class, 'reqAjaxDataPaket'])->name('sistem.data-paket');
    Route::post('/sistem/update-paket', [ManajemenController::class, 'reqAjaxFormUpdatePaket'])->name('sistem.update-paket');
    Route::get('/sistem/data-peserta-pendaftaran', [ManajemenController::class, 'reqAjaxDataPesertaPendaftaran'])->name('sistem.data-peserta-pendaftaran');
    Route::get('/sistem/bukti-pembayaran', [ManajemenController::class, 'reqAjaxBuktiPembayaran'])->name('sistem.bukti-pembayaran');
    Route::get('/sistem/verifikasi-pendaftaran', [ManajemenController::class, 'reqAjaxVerifikasi'])->name('sistem.verifikasi-pendaftaran');
    Route::get('/sistem/hapus-pendaftaran', [ManajemenController::class, 'reqAjaxHapusPendaftaran'])->name('sistem.hapus-pendaftaran');
    
    Route::post('/sistem/tambah-user', [ManajemenController::class, 'reqAjaxFormTambahUser'])->name('sistem.tambah-user');
    Route::get('/sistem/detail-user', [ManajemenController::class, 'reqAjaxDataUser'])->name('sistem.detail-user');
    Route::post('/sistem/update-user', [ManajemenController::class, 'reqAjaxFormUpdateUser'])->name('sistem.update-user');
    
    
    Route::get('/sistem/cetak-laporan/{jenis}', [ManajemenController::class, 'reqCetakLaporan'])->name('sistem.cetak-laporan');

});
